package com.toptal.jogging.contracts;


import com.toptal.jogging.controller.JoggingController;
import com.toptal.jogging.controller.JoggingReportController;
import com.toptal.jogging.controller.UsersController;
import com.toptal.jogging.error.RestExceptionHandler;
import com.toptal.jogging.service.BaseServiceTest;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public abstract class SpringCloudContractBaseTest extends BaseServiceTest {

    @Rule
    public final TestName testName = new TestName();
    private MockMvc mockMvc;
    @Autowired
    private UsersController usersController;

    @Autowired
    private JoggingController joggingController;

    @Autowired
    private JoggingReportController joggingReportController;

    @Before
    public void setup() {
        mockMvc =
            MockMvcBuilders.standaloneSetup(
                usersController,
                joggingController,
                joggingReportController
            ).setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .setControllerAdvice(new RestExceptionHandler()).build();
        RestAssuredMockMvc.mockMvc(mockMvc);

        String methodName = testName.getMethodName();
        if (StringUtils.contains(methodName, "RootUser")) {
            setupMockedRootUser();
        } else if (StringUtils.contains(methodName, "StandardUser")) {
            setupMockedStandardUser();
        } else if (StringUtils.contains(methodName, "ManagerUser")) {
            setupMockedManagerUser();
        } else {
            throw new IllegalArgumentException(
                "Contract test names must finish with 'RootUser' or 'ManagerUser' or 'StandardUser'");
        }
    }

}
