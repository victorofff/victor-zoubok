package com.toptal.jogging.criteria;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.toptal.jogging.criteria.impl.Token;
import com.toptal.jogging.criteria.impl.Token.Type;
import com.toptal.jogging.criteria.impl.Tokenizer;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class TokenizerTest {

    @Test
    public void testTokenizerParenthesis() {
        Tokenizer tokenizer = new Tokenizer("())(");
        List<Token.Type> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .map(Token::getType)
            .collect(Collectors.toList());
        assertThat(results).containsExactly(Type.OPEN_PARENTHESIS, Type.CLOSE_PARENTHESIS, Type.CLOSE_PARENTHESIS,
            Type.OPEN_PARENTHESIS);
    }

    @Test
    public void testTokenizerStringConstant() {
        Tokenizer tokenizer = new Tokenizer("'dummy'");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());

        assertThat(results).hasSize(1);
        assertThat(results.get(0).getType()).isEqualTo(Type.STRING_CONSTANT);
        assertThat(results.get(0).getValue()).isEqualTo("dummy");
    }

    @Test
    public void testTokenizerStringConstantEmptyString() {
        Tokenizer tokenizer = new Tokenizer("''");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());

        assertThat(results).hasSize(1);
        assertThat(results.get(0).getType()).isEqualTo(Type.STRING_CONSTANT);
        assertThat(results.get(0).getValue()).isEqualTo("");
    }

    @Test
    public void testTokenizerNumericalConstant() {
        Tokenizer tokenizer = new Tokenizer("20");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());

        assertThat(results).hasSize(1);
        assertThat(results.get(0).getType()).isEqualTo(Type.NUMERICAL_CONSTANT);
        assertThat(results.get(0).getValue()).isEqualTo("20");
    }

    @Test
    public void testTokenizerDelimitersAtTheEnd() {
        Tokenizer tokenizer = new Tokenizer("30 eq    ");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());
        assertThat(results).hasSize(2);

        assertThat(results.get(0).getType()).isEqualTo(Type.NUMERICAL_CONSTANT);
        assertThat(results.get(0).getValue()).isEqualTo("30");

        assertThat(results.get(1).getType()).isEqualTo(Type.EQ_OPERATOR);
    }

    @Test
    public void testTokenizerOperators() {
        Tokenizer tokenizer = new Tokenizer("eq ne gt lt");
        List<Token.Type> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .map(Token::getType)
            .collect(Collectors.toList());
        assertThat(results).containsExactly(Type.EQ_OPERATOR, Type.NE_OPERATOR, Type.GT_OPERATOR,
            Type.LT_OPERATOR);
    }

    @Test
    public void testTokenizerFirstOperands() {
        Tokenizer tokenizer = new Tokenizer("field1 field2");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());
        assertThat(results).hasSize(2);

        assertThat(results.get(0).getType()).isEqualTo(Type.FIELD);
        assertThat(results.get(0).getValue()).isEqualTo("field1");

        assertThat(results.get(1).getType()).isEqualTo(Type.FIELD);
        assertThat(results.get(1).getValue()).isEqualTo("field2");
    }

    @Test
    public void testTokenizerAndOrOperators() {
        Tokenizer tokenizer = new Tokenizer("or and");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());

        assertThat(results).hasSize(2);

        assertThat(results.get(0).getType()).isEqualTo(Type.OR_OPERATOR);
        assertThat(results.get(1).getType()).isEqualTo(Type.AND_OPERATOR);
    }
    @Test
    public void testTokenizerSpecificDate() {
        Tokenizer tokenizer = new Tokenizer("timeStamp eq '2020-01-09 01:52:46.269'");
        List<Token> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .collect(Collectors.toList());

        assertThat(results).hasSize(3);
        assertThat(results.get(0).getType()).isEqualTo(Type.FIELD);
        assertThat(results.get(0).getValue()).isEqualTo("timeStamp");

        assertThat(results.get(1).getType()).isEqualTo(Type.EQ_OPERATOR);

        assertThat(results.get(2).getType()).isEqualTo(Type.STRING_CONSTANT);
        assertThat(results.get(2).getValue()).isEqualTo("2020-01-09 01:52:46.269");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTokenizerUnterminatedStringConstant() {
        Tokenizer tokenizer = new Tokenizer("timeStamp eq '2020-01-09 0");
        tokenizer.forEach(token -> assertThat(token).isNotNull());
    }

    @Test
    public void testTokenizerComplex() {
        Tokenizer tokenizer = new Tokenizer("(date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10))");
        List<Token.Type> results = StreamSupport.stream(tokenizer.spliterator(), false)
            .map(Token::getType)
            .collect(Collectors.toList());

        assertThat(results)
            .containsExactly(Type.OPEN_PARENTHESIS, Type.FIELD, Type.EQ_OPERATOR, Type.STRING_CONSTANT,
                Type.CLOSE_PARENTHESIS, Type.AND_OPERATOR, Type.OPEN_PARENTHESIS, Type.OPEN_PARENTHESIS, Type.FIELD,
                Type.GT_OPERATOR, Type.NUMERICAL_CONSTANT, Type.CLOSE_PARENTHESIS, Type.OR_OPERATOR,
                Type.OPEN_PARENTHESIS, Type.FIELD, Type.LT_OPERATOR, Type.NUMERICAL_CONSTANT, Type.CLOSE_PARENTHESIS,
                Type.CLOSE_PARENTHESIS);
    }
}