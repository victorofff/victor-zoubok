package com.toptal.jogging.service;

import java.time.format.DateTimeFormatter;

import com.toptal.jogging.TestDatasourceConfig;
import com.toptal.jogging.TestPasswordEncoderConfig;
import com.toptal.jogging.controller.JoggingController;
import com.toptal.jogging.controller.JoggingReportController;
import com.toptal.jogging.controller.UsersController;
import com.toptal.jogging.criteria.impl.CriteriaParserImpl;
import com.toptal.jogging.domain.Role;
import com.toptal.jogging.domain.User;
import com.toptal.jogging.repository.UserRepository;
import com.toptal.jogging.service.impl.JoggingReportServiceImpl;
import com.toptal.jogging.service.impl.JoggingServiceImpl;
import com.toptal.jogging.service.impl.UserServiceImpl;
import com.toptal.jogging.service.impl.WeatherServiceImpl;
import com.toptal.jogging.util.TestConst;
import liquibase.exception.LiquibaseException;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = {
    TestDatasourceConfig.class,
    UserServiceImpl.class,
    JoggingReportServiceImpl.class,
    WeatherServiceImpl.class,
    CriteriaParserImpl.class,
    JoggingServiceImpl.class,
    UsersController.class,
    JoggingController.class,
    JoggingReportController.class,
    TestPasswordEncoderConfig.class
})
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@Transactional
@MockBeans({
    @MockBean(RestTemplate.class)
})
public abstract class BaseServiceTest extends BaseTest {

    @MockBean
    private AuthenticationService authenticationService;

    @Autowired
    private UserRepository userRepository;

    @Value("${criteria.timestamp.format:yyyy-MM-dd HH:mm:ss.SSS}")
    private String timeStampFormatString;

    protected DateTimeFormatter timeStampFormatter;
    protected User rootUser;
    protected User managerUser;
    protected User standardUser;


    @Before
    public void initialize() throws LiquibaseException {
        applyLatestChanges();

        standardUser = userRepository.findByUserName(TestConst.STANDARD_USER_NAME);
        assertThat(standardUser).isNotNull();
        assertThat(standardUser.getRoles().stream().map(Role::getRoleName)
            .filter(TestConst.STANDARD_USER_ROLE::equals).count()).isEqualTo(1);

        managerUser = userRepository.findByUserName(TestConst.MANAGER_USER_NAME);
        assertThat(managerUser).isNotNull();
        assertThat(managerUser.getRoles().stream().map(Role::getRoleName)
            .filter(TestConst.MANAGER_USER_ROLE::equals).count()).isEqualTo(1);

        rootUser = userRepository.findByUserName(TestConst.ADMIN_USER_NAME);
        assertThat(rootUser).isNotNull();
        assertThat(rootUser.getRoles().stream().map(Role::getRoleName)
            .filter(TestConst.ADMIN_USER_ROLE::equals).count()).isEqualTo(1);

        timeStampFormatter = DateTimeFormatter.ofPattern(timeStampFormatString);
    }

    public void setupMockedManagerUser() {
        given(authenticationService.hasPrivilege("CRUD_OWN_USERS")).willReturn(false);
        given(authenticationService.hasPrivilege("CRUD_ALL_RECORDS")).willReturn(false);
        given(authenticationService.hasPrivilege("CRUD_ALL_USERS")).willReturn(true);
        given(authenticationService.getUser()).willReturn(managerUser);
    }

    public void setupMockedStandardUser() {
        given(authenticationService.hasPrivilege("CRUD_OWN_USERS")).willReturn(true);
        given(authenticationService.hasPrivilege("CRUD_ALL_RECORDS")).willReturn(false);
        given(authenticationService.hasPrivilege("CRUD_ALL_USERS")).willReturn(false);
        given(authenticationService.getUser()).willReturn(standardUser);
    }

    public void setupMockedRootUser() {
        given(authenticationService.hasPrivilege("CRUD_OWN_USERS")).willReturn(true);
        given(authenticationService.hasPrivilege("CRUD_ALL_USERS")).willReturn(true);
        given(authenticationService.hasPrivilege("CRUD_ALL_RECORDS")).willReturn(true);
        given(authenticationService.getUser()).willReturn(rootUser);
    }


}
