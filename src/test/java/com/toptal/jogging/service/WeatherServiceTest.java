package com.toptal.jogging.service;


import java.time.LocalDateTime;

import com.toptal.jogging.dto.LocationDto;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

public class WeatherServiceTest extends BaseServiceTest {

    private final static LocationDto GOOD_LOCATION_DTO = new LocationDto(1.0, 2.0);
    private final static LocationDto BAD_LOCATION_DTO = new LocationDto(4.0, 5.0);
    private final static String GOOD_LOCATION_RESPONSE = "{ good }";

    @Value("${wheather.api.service.url}")
    private String serviceUrl;

    @Value("${whether.api.key}")
    private String apiKey;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private WeatherService weatherService;

    @Before
    public void setup() {
        given(restTemplate.getForObject(UriComponentsBuilder.fromHttpUrl(serviceUrl)
            .queryParam("lat", GOOD_LOCATION_DTO.getLatitude())
            .queryParam("lon", GOOD_LOCATION_DTO.getLongitude())
            .queryParam("appid", apiKey)
            .toUriString(), String.class)).willReturn(GOOD_LOCATION_RESPONSE);

        given(restTemplate.getForObject(UriComponentsBuilder.fromHttpUrl(serviceUrl)
            .queryParam("lat", BAD_LOCATION_DTO.getLatitude())
            .queryParam("lon", BAD_LOCATION_DTO.getLongitude())
            .queryParam("appid", apiKey)
            .toUriString(), String.class)).willThrow(RestClientException.class);
    }


    @Test
    public void testWeatherServiceCorrectResponse() {
        String result = weatherService.getWeather(GOOD_LOCATION_DTO, LocalDateTime.now());
        assertThat(result).isEqualTo(GOOD_LOCATION_RESPONSE);
    }

    @Test
    public void testWeatherServiceBadResponse() {
        String result = weatherService.getWeather(BAD_LOCATION_DTO, LocalDateTime.now());
        assertThat(result).isEmpty();
    }
}