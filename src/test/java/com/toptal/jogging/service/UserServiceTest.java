package com.toptal.jogging.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.toptal.jogging.request.UserRequest;
import com.toptal.jogging.response.UserResponse;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;


public class UserServiceTest extends BaseServiceTest {

    private static final long INVALID_USER_ID = -2;

    @Autowired
    private UserService userService;

    @Test
    public void testFindAllPageable() {
        setupMockedRootUser();
        Page<UserResponse> page1 = userService.findAllPageable(PageRequest.of(0, 1));
        Page<UserResponse> page2 = userService.findAllPageable(PageRequest.of(1, 1));
        Page<UserResponse> page3 = userService.findAllPageable(PageRequest.of(2, 1));

        assertThat(page1.getSize()).isEqualTo(1);
        assertThat(page2.getSize()).isEqualTo(1);
        assertThat(page3.getSize()).isEqualTo(1);

        assertThat(Stream.concat(Stream.concat(page1.get(), page2.get()), page3.get())
            .map(UserResponse::getUserName)
            .collect(Collectors.toList())).containsExactlyInAnyOrder("john.doe", "bob.frapples", "bill.robson");
    }

    @Test
    public void testFindAll() {
        setupMockedRootUser();
        List<UserResponse> allData = userService.findAll();
        assertThat(allData.stream().map(UserResponse::getUserName).collect(Collectors.toList()))
            .containsExactlyInAnyOrder("john.doe", "bob.frapples", "bill.robson");
    }

    @Test(expected = NonAuthorizedException.class)
    public void testFindAllNonAdminUser() {
        setupMockedManagerUser();
        userService.findAll();
    }

    @Test
    public void testFindById() {
        setupMockedRootUser();
        UserResponse userResponse = userService.findById(rootUser.getId());
        assertThat(userResponse.getUserName()).isEqualTo("bob.frapples");
        assertThat(userResponse.getFirstName()).isEqualTo("Bob");
        assertThat(userResponse.getLastName()).isEqualTo("Frapples");

        assertThat(userResponse.getRoles()).containsExactlyInAnyOrder("ADMIN_USER");
    }

    @Test
    public void testSave() {
        setupMockedRootUser();
        UserRequest userRequest =
            UserRequest.builder().userName("alice.cooper").firstName("Alice").lastName("Cooper").password(
                "passwpord1").build();
        userRequest.getRoles().addAll(Arrays.asList("STANDARD_USER", "MANAGER_USER"));
        UserResponse userResponse = userService.save(userRequest);

        assertThat(userResponse.getUserName()).isEqualTo("alice.cooper");
        assertThat(userResponse.getFirstName()).isEqualTo("Alice");
        assertThat(userResponse.getLastName()).isEqualTo("Cooper");

        assertThat(userResponse.getRoles()).containsExactlyInAnyOrder("STANDARD_USER", "MANAGER_USER");
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSaveExistingUser() {
        setupMockedRootUser();

        UserRequest userRequest =
            UserRequest.builder().userName("john.doe").firstName("John").lastName("Doe").password(
                "passwpord1").build();
        userRequest.getRoles().add("STANDARD_USER");
        userService.save(userRequest);
    }

    @Test(expected = NonAuthorizedException.class)
    public void testSaveNonAdminUser() {
        setupMockedStandardUser();
        UserRequest userRequest =
            UserRequest.builder().userName("alice.cooper").firstName("Alice").lastName("Cooper").password(
                "passwpord1").build();
        userRequest.getRoles().addAll(Arrays.asList("STANDARD_USER", "MANAGER_USER"));
        userService.save(userRequest);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testSaveInvalidRoles() {
        setupMockedRootUser();
        UserResponse userResponse = userService.findById(standardUser.getId());
        assertThat(userResponse.getUserName()).isEqualTo("john.doe");
        assertThat(userResponse.getFirstName()).isEqualTo("John");
        assertThat(userResponse.getLastName()).isEqualTo("Doe");

        assertThat(userResponse.getRoles()).containsExactly("STANDARD_USER");
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName("John2");
        userRequest.setLastName("Doe2");
        userRequest.setUserName("john2.doe2");
        userRequest.setPassword("password123");
        userRequest.getRoles().addAll(Arrays.asList("STANDARD_USER", "INVALID_ROLE"));

        userService.update(1, userRequest);
    }

    @Test
    public void testUpdate() {
        setupMockedRootUser();
        UserResponse userResponse = userService.findById(standardUser.getId());
        long originalCount = userService.findAll().size();
        assertThat(userResponse.getUserName()).isEqualTo("john.doe");
        assertThat(userResponse.getFirstName()).isEqualTo("John");
        assertThat(userResponse.getLastName()).isEqualTo("Doe");

        assertThat(userResponse.getRoles()).containsExactly("STANDARD_USER");
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName("John2");
        userRequest.setLastName("Doe2");
        userRequest.setUserName("john2.doe2");
        userRequest.setPassword("password123");
        userRequest.getRoles().addAll(Arrays.asList("STANDARD_USER", "MANAGER_USER"));

        UserResponse updatedUserResponse = userService.update(standardUser.getId(), userRequest);
        long updatedCount = userService.findAll().size();
        assertThat(originalCount).isEqualTo(updatedCount);
        assertThat(updatedUserResponse.getUserName()).isEqualTo("john2.doe2");
        assertThat(updatedUserResponse.getFirstName()).isEqualTo("John2");
        assertThat(updatedUserResponse.getLastName()).isEqualTo("Doe2");

        assertThat(updatedUserResponse.getRoles()).containsExactlyInAnyOrder("STANDARD_USER", "MANAGER_USER");
    }

    @Test
    public void testUpdateSameUserName() {
        setupMockedRootUser();
        UserResponse userResponse = userService.findById(standardUser.getId());
        long originalCount = userService.findAll().size();
        assertThat(userResponse.getUserName()).isEqualTo("john.doe");
        assertThat(userResponse.getFirstName()).isEqualTo("John");
        assertThat(userResponse.getLastName()).isEqualTo("Doe");

        assertThat(userResponse.getRoles()).containsExactly("STANDARD_USER");
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName("John2");
        userRequest.setLastName("Doe2");
        userRequest.setUserName("john.doe");
        userRequest.setPassword("password123");
        userRequest.getRoles().addAll(Arrays.asList("STANDARD_USER", "MANAGER_USER"));

        UserResponse updatedUserResponse = userService.update(standardUser.getId(), userRequest);
        long updatedCount = userService.findAll().size();
        assertThat(originalCount).isEqualTo(updatedCount);
        assertThat(updatedUserResponse.getUserName()).isEqualTo("john.doe");
        assertThat(updatedUserResponse.getFirstName()).isEqualTo("John2");
        assertThat(updatedUserResponse.getLastName()).isEqualTo("Doe2");

        assertThat(updatedUserResponse.getRoles()).containsExactlyInAnyOrder("STANDARD_USER", "MANAGER_USER");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateMissingUser() {
        setupMockedRootUser();
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName("John2");
        userRequest.setLastName("Doe2");
        userRequest.setUserName("john2.doe2");
        userRequest.setPassword("password123");
        userRequest.getRoles().addAll(Arrays.asList("STANDARD_USER", "MANAGER_USER"));

        userService.update(INVALID_USER_ID, userRequest);
    }

    @Test
    public void testDeleteById() {
        setupMockedRootUser();
        UserResponse userResponse = userService.findById(standardUser.getId());
        assertThat(userResponse.getUserName()).isEqualTo("john.doe");
        assertThat(userResponse.getFirstName()).isEqualTo("John");
        assertThat(userResponse.getLastName()).isEqualTo("Doe");

        userService.deleteById(1);
        assertThat(catchThrowable(() -> userService.findById(1))).isInstanceOf(EntityNotFoundException.class);
    }

    @Test(expected = NonAuthorizedException.class)
    public void testDeleteByIdNonAdminUser() {
        setupMockedStandardUser();
        userService.deleteById(standardUser.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteByIdInvalidId() {
        setupMockedRootUser();
        userService.deleteById(INVALID_USER_ID);
    }
}