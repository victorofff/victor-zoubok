package com.toptal.jogging.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import com.toptal.jogging.domain.Jogging;
import com.toptal.jogging.dto.LocationDto;
import com.toptal.jogging.repository.JoggingRepository;
import com.toptal.jogging.request.JoggingEntryRequest;
import com.toptal.jogging.response.JoggingEntryResponse;
import com.toptal.jogging.util.TestConst;
import com.toptal.jogging.util.TestUtility;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

public class JoggingServiceTest extends BaseServiceTest {

    @Autowired
    private JoggingService joggingService;

    @Autowired
    private JoggingRepository joggingRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testFindAllStandardUserNoFilter() {
        setupMockedStandardUser();

        List<JoggingEntryResponse> userJoggings = joggingService.findAll("");
        List<Jogging> repositoryUserRecords = joggingRepository.findAll().stream()
            .filter(jogging -> standardUser.getUserName().equals(jogging.getUser().getUserName()))
            .collect(Collectors.toList());

        assertThat(userJoggings.size()).isEqualTo(repositoryUserRecords.size());
        assertThat(userJoggings.stream().map(JoggingEntryResponse::getDistance)
            .collect(Collectors.toList()))
            .containsExactlyInAnyOrder(
                repositoryUserRecords.stream().map(Jogging::getDistance).toArray(Integer[]::new));
    }

    @Test
    public void testFindAllManagerUserNoFilter() {
        setupMockedManagerUser();

        List<JoggingEntryResponse> allJoggings = joggingService.findAll("");
        List<Jogging> repositoryAllRecord = new ArrayList<>(joggingRepository.findAll());

        assertThat(allJoggings.size()).isEqualTo(repositoryAllRecord.size());
        assertThat(allJoggings.stream().map(JoggingEntryResponse::getDistance)
            .collect(Collectors.toList()))
            .containsExactlyInAnyOrder(
                repositoryAllRecord.stream().map(Jogging::getDistance).toArray(Integer[]::new));
    }

    @Test
    public void testFindAllStandardUserPageableNoFilter() {
        setupMockedStandardUser();

        List<JoggingEntryResponse> userJoggings = TestUtility.getJoggingPagedContent(joggingService, "");
        List<Jogging> repositoryUserRecords = joggingRepository.findAll().stream()
            .filter(jogging -> standardUser.getUserName().equals(jogging.getUser().getUserName()))
            .collect(Collectors.toList());

        assertThat(userJoggings.size()).isEqualTo(repositoryUserRecords.size());
        assertThat(userJoggings.stream().map(JoggingEntryResponse::getDistance)
            .collect(Collectors.toList()))
            .containsExactlyInAnyOrder(
                repositoryUserRecords.stream().map(Jogging::getDistance).toArray(Integer[]::new));
    }

    @Test
    public void testFindAllManagerUserPageableNoFilter() {
        setupMockedManagerUser();

        List<JoggingEntryResponse> allJoggings = TestUtility.getJoggingPagedContent(joggingService, "");
        List<Jogging> repositoryAllRecord = new ArrayList<>(joggingRepository.findAll());

        assertThat(allJoggings.size()).isEqualTo(repositoryAllRecord.size());
        assertThat(allJoggings.stream().map(JoggingEntryResponse::getDistance)
            .collect(Collectors.toList()))
            .containsExactlyInAnyOrder(
                repositoryAllRecord.stream().map(Jogging::getDistance).toArray(Integer[]::new));
    }

    @Test
    public void testSaveStandardUser() {
        setupMockedStandardUser();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .locationDto(new LocationDto(35.7888, 546.668))
            .distance(245)
            .localDateTime(LocalDateTime.now()).build();

        JoggingEntryResponse response = joggingService.save(joggingEntryRequest);
        assertThat(response).isNotNull();
        assertThat(response.getUserName()).isEqualTo(standardUser.getUserName());
        TestUtility
            .assertDoublesReasonablyEqual(response.getLocationDto().getLatitude(), 35.7888);
        TestUtility
            .assertDoublesReasonablyEqual(response.getLocationDto().getLongitude(), 546.668);
        assertThat(response.getDistance()).isEqualTo(245);
    }

    @Test
    public void testSaveManagerUser() {
        setupMockedManagerUser();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .locationDto(new LocationDto(35.7888, 546.668))
            .distance(255)
            .localDateTime(LocalDateTime.now()).build();

        JoggingEntryResponse response = joggingService.save(joggingEntryRequest);
        assertThat(response).isNotNull();
        assertThat(response.getUserName()).isEqualTo(managerUser.getUserName());
        TestUtility
            .assertDoublesReasonablyEqual(response.getLocationDto().getLatitude(), 35.7888);
        TestUtility
            .assertDoublesReasonablyEqual(response.getLocationDto().getLongitude(), 546.668);
        assertThat(response.getDistance()).isEqualTo(255);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSaveStandardUserExistingRecord() {
        setupMockedStandardUser();

        LocalDateTime localDateTime = LocalDateTime
            .parse("2020-02-08 01:52:46.269",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .locationDto(new LocationDto(0.1, 0.2))
            .distance(3)
            .localDateTime(localDateTime)
            .build();

        joggingService.save(joggingEntryRequest);
    }

    @Test(expected = PersistenceException.class)
    public void testUpdateStandardUserExistingRecord() {
        setupMockedStandardUser();

        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_STANDARD_USER);

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .localDateTime(joggingEntryResponse.getLocalDateTime())
            .distance(joggingEntryResponse.getDistance() + 101)
            .locationDto(
                new LocationDto(
                    joggingEntryResponse.getLocationDto().getLatitude() + 0.001,
                    joggingEntryResponse.getLocationDto().getLongitude() + 0.003))
            .build();

        joggingService
            .update(TestConst.JOGGING_ID_ANOTHER_STANDARD_USER, joggingEntryRequest);

        entityManager.flush();

    }


    @Test
    public void testUpdateStandardUserStandardUserRecord() {
        setupMockedStandardUser();

        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_STANDARD_USER);
        long countBefore = joggingRepository.findAll().size();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .localDateTime(LocalDateTime.now())
            .distance(joggingEntryResponse.getDistance() + 101)
            .locationDto(
                new LocationDto(
                    joggingEntryResponse.getLocationDto().getLatitude() + 0.001,
                    joggingEntryResponse.getLocationDto().getLongitude() + 0.003))
            .build();

        JoggingEntryResponse updated = joggingService
            .update(TestConst.JOGGING_ID_STANDARD_USER, joggingEntryRequest);
        long countAfter = joggingRepository.findAll().size();
        assertThat(countBefore).isEqualTo(countAfter);
        assertThat(updated).isNotNull();
        assertThat(updated.getDistance()).isEqualTo(joggingEntryResponse.getDistance() + 101);

        TestUtility.assertDoublesReasonablyEqual(updated.getLocationDto().getLatitude(),
            joggingEntryResponse.getLocationDto().getLatitude() + 0.001);
        TestUtility.assertDoublesReasonablyEqual(updated.getLocationDto().getLongitude(),
            joggingEntryResponse.getLocationDto().getLongitude() + 0.003);
    }


    @Test
    public void testUpdateManagerUserManagerUserRecord() {
        setupMockedManagerUser();

        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_MANAGER_USER);
        long countBefore = joggingRepository.findAll().size();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .localDateTime(LocalDateTime.now())
            .distance(joggingEntryResponse.getDistance() + 102)
            .locationDto(
                new LocationDto(
                    joggingEntryResponse.getLocationDto().getLatitude() + 0.0056,
                    joggingEntryResponse.getLocationDto().getLongitude() + 0.004))
            .build();

        JoggingEntryResponse updated = joggingService
            .update(TestConst.JOGGING_ID_MANAGER_USER, joggingEntryRequest);
        long countAfter = joggingRepository.findAll().size();
        assertThat(countBefore).isEqualTo(countAfter);
        assertThat(updated).isNotNull();
        assertThat(updated.getDistance()).isEqualTo(joggingEntryResponse.getDistance() + 102);

        TestUtility.assertDoublesReasonablyEqual(updated.getLocationDto().getLatitude(),
            joggingEntryResponse.getLocationDto().getLatitude() + 0.0056);
        TestUtility.assertDoublesReasonablyEqual(updated.getLocationDto().getLongitude(),
            joggingEntryResponse.getLocationDto().getLongitude() + 0.004);
    }

    @Test
    public void testUpdateManagerUserStandardUserRecord() {
        setupMockedManagerUser();

        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_STANDARD_USER);
        long countBefore = joggingRepository.findAll().size();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .localDateTime(LocalDateTime.now())
            .distance(joggingEntryResponse.getDistance() + 104)
            .locationDto(
                new LocationDto(
                    joggingEntryResponse.getLocationDto().getLatitude() + 0.008,
                    joggingEntryResponse.getLocationDto().getLongitude() + 0.0035))
            .build();

        JoggingEntryResponse updated = joggingService
            .update(TestConst.JOGGING_ID_STANDARD_USER, joggingEntryRequest);
        long countAfter = joggingRepository.findAll().size();
        assertThat(countBefore).isEqualTo(countAfter);
        assertThat(updated).isNotNull();
        assertThat(updated.getDistance()).isEqualTo(joggingEntryResponse.getDistance() + 104);

        TestUtility.assertDoublesReasonablyEqual(updated.getLocationDto().getLatitude(),
            joggingEntryResponse.getLocationDto().getLatitude() + 0.008);
        TestUtility.assertDoublesReasonablyEqual(updated.getLocationDto().getLongitude(),
            joggingEntryResponse.getLocationDto().getLongitude() + 0.0035);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateInvalidIdStandardUser() {
        setupMockedStandardUser();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .localDateTime(LocalDateTime.now())
            .distance(146)
            .locationDto(
                new LocationDto(
                    23.64,
                    75.767))
            .build();

        joggingService
            .update(TestConst.JOGGING_ID_INVALID, joggingEntryRequest);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateInvalidIdManagerUser() {
        setupMockedManagerUser();

        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .localDateTime(LocalDateTime.now())
            .distance(149)
            .locationDto(
                new LocationDto(
                    54.56,
                    48.49))
            .build();

        joggingService
            .update(TestConst.JOGGING_ID_INVALID, joggingEntryRequest);
    }

    @Test
    public void testDeleteByIdStandardUserStandardUserRecord() {
        setupMockedStandardUser();
        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_STANDARD_USER);

        assertThat(joggingEntryResponse.getUserName()).isEqualTo(standardUser.getUserName());
        joggingService.deleteById(TestConst.JOGGING_ID_STANDARD_USER);
        assertThat(catchThrowable(() -> joggingService.findById(TestConst.JOGGING_ID_STANDARD_USER)))
            .isInstanceOf(EntityNotFoundException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteByIdStandardUserNoPermissions() {
        setupMockedStandardUser();
        joggingService.deleteById(TestConst.JOGGING_ID_MANAGER_USER);
    }

    @Test
    public void testDeleteByIdManagerUserManagerUserRecord() {
        setupMockedManagerUser();

        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_MANAGER_USER);
        assertThat(joggingEntryResponse.getUserName()).isEqualTo(managerUser.getUserName());

        joggingService.deleteById(TestConst.JOGGING_ID_MANAGER_USER);
        assertThat(catchThrowable(() -> joggingService.findById(TestConst.JOGGING_ID_MANAGER_USER)))
            .isInstanceOf(EntityNotFoundException.class);
    }

    @Test
    public void testDeleteByIdManagerUserStandardUserRecord() {
        setupMockedManagerUser();
        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_STANDARD_USER);

        assertThat(joggingEntryResponse.getUserName()).isEqualTo(standardUser.getUserName());
        joggingService.deleteById(TestConst.JOGGING_ID_STANDARD_USER);
        assertThat(catchThrowable(() -> joggingService.findById(TestConst.JOGGING_ID_STANDARD_USER)))
            .isInstanceOf(EntityNotFoundException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteByIdInvalidIdStandardUser() {
        setupMockedStandardUser();
        joggingService.deleteById(TestConst.JOGGING_ID_INVALID);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteByIdInvalidIdManagerUser() {
        setupMockedManagerUser();
        joggingService.deleteById(TestConst.JOGGING_ID_INVALID);
    }

    @Test
    public void testFindByIdStandardUser() {
        setupMockedStandardUser();
        JoggingEntryResponse joggingEntryResponse = joggingService
            .findById(TestConst.JOGGING_ID_STANDARD_USER);
        assertThat(joggingEntryResponse.getUserName()).isEqualTo(standardUser.getUserName());
    }

}