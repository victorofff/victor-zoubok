package com.toptal.jogging.service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.toptal.jogging.dto.LocationDto;
import com.toptal.jogging.response.JoggingEntryResponse;
import com.toptal.jogging.util.TestUtility;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

public class JoggingServiceFilterTest extends BaseServiceTest {

    @Autowired
    private JoggingService joggingService;

    @Test
    public void testFindAllStandardUserSingleDistanceEqualFilter() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggings = joggingService.findAll("distance eq 22");

        assertThat(userJoggings).hasSize(1);

        JoggingEntryResponse joggingEntryResponse = userJoggings.get(0);
        assertThat(joggingEntryResponse.getUserName()).isEqualTo(standardUser.getUserName());
        assertThat(joggingEntryResponse.getDistance()).isEqualTo(22);

        LocationDto locationDto = joggingEntryResponse.getLocationDto();
        TestUtility.assertDoublesReasonablyEqual(locationDto.getLatitude(), 0.9);
        TestUtility.assertDoublesReasonablyEqual(locationDto.getLongitude(), 1.1);
    }

    @Test
    public void testFindAllStandardUserDistanceLessFilter() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried = joggingService.findAll("distance LT 22");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(jogging -> jogging.getDistance() <= 22)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceLessFilterPaging() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried = TestUtility.getJoggingPagedContent(joggingService, "distance lt 22");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(jogging -> jogging.getDistance() <= 22)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceMoreFilter() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried = joggingService.findAll("distance gt 22");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(jogging -> jogging.getDistance() >= 22)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    public void testFindAllStandardUserDistanceMoreFilterPaging() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried = TestUtility.getJoggingPagedContent(joggingService, "distance gt 22");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(jogging -> jogging.getDistance() >= 22)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceTwoOrFilters() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried = joggingService.findAll("(distance gt 22) OR (distance lt 4)");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream()
                .filter(jogging -> jogging.getDistance() >= 22 || jogging.getDistance() <= 4)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceTwoAndFilters() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried =
            joggingService.findAll("( (distance gt 4) AND (distance lt 22) )");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream()
                .filter(jogging -> jogging.getDistance() <= 22 && jogging.getDistance() >= 4)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceNeTwoAndFilters() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried =
            joggingService.findAll("(distance ne 4) AND (distance ne 22)");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream()
                .filter(jogging -> jogging.getDistance() != 22 && jogging.getDistance() != 4)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceAndOrFilters() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried =
            joggingService.findAll("(distance eq 3) or ((distance gt 4) AND (distance lt 22))");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(
                jogging -> jogging.getDistance() == 3 || (jogging.getDistance() <= 22 && jogging.getDistance() >= 4)
            ).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceAndOrFiltersPaging() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried =
            TestUtility.getJoggingPagedContent(joggingService, "(distance eq 3) or ((distance gt 4) AND (distance lt 22))");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(
                jogging -> jogging.getDistance() == 3 || (jogging.getDistance() <= 22 && jogging.getDistance() >= 4)
            ).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceAndOrFiltersNoOrParenthesesNaturalOrder() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried =
            joggingService.findAll("distance eq 3 or (distance gt 4 AND distance lt 22)");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(
                jogging -> jogging.getDistance() == 3 || (jogging.getDistance() <= 22 && jogging.getDistance() >= 4)
            ).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDistanceAndOrFiltersNoAllParenthesesNaturalOrder() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggingsQueried =
            joggingService.findAll("distance eq 3 or distance gt 4 AND distance lt 22");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(
                jogging -> jogging.getDistance() == 3 || jogging.getDistance() <= 22 && jogging.getDistance() >= 4
            ).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }


    @Test
    public void testFindAllStandardUserDistanceTimeStampAndOrFiltersNoAllParenthesesNaturalOrder() {
        setupMockedStandardUser();

        String dateTimeThresholdBegin = "2020-02-11 01:52:46.269";
        String dateTimeThresholdEnd = "2020-01-09 01:52:46.269";

        LocalDateTime filterDateTimeBegin = LocalDateTime.parse(dateTimeThresholdBegin, timeStampFormatter);
        LocalDateTime filterDateTimeEnd = LocalDateTime.parse(dateTimeThresholdEnd, timeStampFormatter);

        List<JoggingEntryResponse> userJoggingsQueried =
            joggingService.findAll("distance eq 3 or timeStamp gt '" + dateTimeThresholdBegin +
                "' and timeStamp lt '" + dateTimeThresholdEnd + "'");

        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(
                jogging -> jogging.getDistance() == 3 ||
                    jogging.getLocalDateTime().compareTo(filterDateTimeBegin) >= 0 && jogging.getLocalDateTime().compareTo(filterDateTimeEnd) <= 0
            ).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }




    @Test
    public void testFindAllStandardUserSingleDateEqualFilter() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggings = joggingService.findAll("timeStamp eq '2020-01-09 01:52:46.269'");
        assertThat(userJoggings).hasSize(1);

        JoggingEntryResponse joggingEntryResponse = userJoggings.get(0);
        assertThat(joggingEntryResponse.getUserName()).isEqualTo(standardUser.getUserName());
        assertThat(joggingEntryResponse.getDistance()).isEqualTo(27);

        LocationDto locationDto = joggingEntryResponse.getLocationDto();
        TestUtility.assertDoublesReasonablyEqual(locationDto.getLatitude(), 1.4);
        TestUtility.assertDoublesReasonablyEqual(locationDto.getLongitude(), 1.5);
    }

    @Test
    public void testFindAllStandardUserDateMoreFilter() {
        setupMockedStandardUser();

        String dateTimeThreshold = "2020-01-09 01:52:46.269";
        LocalDateTime filterDateTime = LocalDateTime.parse(dateTimeThreshold, timeStampFormatter);

        List<JoggingEntryResponse> userJoggingsQueried = joggingService
            .findAll("timeStamp gt '" + dateTimeThreshold + "'");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream()
                .filter(jogging -> jogging.getLocalDateTime().compareTo(filterDateTime) >= 0)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserDateLessFilter() {
        setupMockedStandardUser();

        String dateTimeThreshold = "2020-01-09 01:52:46.269";
        LocalDateTime filterDateTime = LocalDateTime.parse(dateTimeThreshold, timeStampFormatter);

        List<JoggingEntryResponse> userJoggingsQueried = joggingService
            .findAll("timeStamp lt '" + dateTimeThreshold + "'");
        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream()
                .filter(jogging -> jogging.getLocalDateTime().compareTo(filterDateTime) <= 0)
                .collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllManagerUserSingleWeatherEqualFilter() {
        setupMockedManagerUser();
        List<JoggingEntryResponse> userJoggings
            = joggingService.findAll("weatherResponse eq '{\"weather\":\"hurricane\"}'");

        assertThat(userJoggings).hasSize(2);

        JoggingEntryResponse joggingEntryResponse1 = userJoggings.get(0);
        JoggingEntryResponse joggingEntryResponse2 = userJoggings.get(1);

        assertThat(joggingEntryResponse1.getUserName()).isEqualTo(managerUser.getUserName());
        assertThat(joggingEntryResponse1.getDistance()).isEqualTo(4);

        assertThat(joggingEntryResponse2.getUserName()).isEqualTo(managerUser.getUserName());
        assertThat(joggingEntryResponse2.getDistance()).isEqualTo(5);

        LocationDto locationDto1 = joggingEntryResponse1.getLocationDto();
        TestUtility.assertDoublesReasonablyEqual(locationDto1.getLatitude(), 100.0);
        TestUtility.assertDoublesReasonablyEqual(locationDto1.getLongitude(), 101.1);

        LocationDto locationDto2 = joggingEntryResponse2.getLocationDto();
        TestUtility.assertDoublesReasonablyEqual(locationDto2.getLatitude(), 102.0);
        TestUtility.assertDoublesReasonablyEqual(locationDto2.getLongitude(), 103.1);
    }

    @Test
    public void testFindAllManagerUserSingleWeatherMoreFilter() {
        setupMockedManagerUser();

        String filter = "{\"weather\":\"hurricane\"}";
        List<JoggingEntryResponse> userJoggingsQueried
            = joggingService.findAll("weatherResponse gt '" + filter + "'");

        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(jogging -> Objects.nonNull(jogging.getWeatherJson()))
                .filter(jogging -> filter.compareTo(jogging.getWeatherJson()) <= 0).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllManagerUserSingleWeatherLessFilter() {
        setupMockedManagerUser();

        String filter = "{\"weather\":\"hurricane\"}";
        List<JoggingEntryResponse> userJoggingsQueried
            = joggingService.findAll("weatherResponse lt '" + filter + "'");

        List<JoggingEntryResponse> userJoggingsFiltered =
            joggingService.findAll("").stream().filter(jogging -> Objects.nonNull(jogging.getWeatherJson()))
                .filter(jogging -> filter.compareTo(jogging.getWeatherJson()) >= 0).collect(Collectors.toList());

        assertTwoListResponsesAreSame(userJoggingsQueried, userJoggingsFiltered);
    }

    @Test
    public void testFindAllStandardUserManagerUserRecords() {
        setupMockedStandardUser();
        List<JoggingEntryResponse> userJoggings
            = joggingService.findAll("weatherResponse eq '{\"weather\":\"hurricane\"}'");
        assertThat(userJoggings).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAllStandardUserInvalidOperator() {
        setupMockedStandardUser();
        joggingService.findAll("weatherResponse invalid '{\"weather\":\"hurricane\"}'");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAllStandardUserInvalidField() {
        setupMockedStandardUser();
        joggingService.findAll("dummyField eq 'dummyValue'");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAllStandardUserInvalidType() {
        setupMockedStandardUser();
        joggingService.findAll("dummyFieldToBeProcessed eq 'dummyValue'");
    }

    private static void assertTwoListResponsesAreSame(List<JoggingEntryResponse> userJoggingsFiltered,
        List<JoggingEntryResponse> userJoggingsQueried) {

        List<JoggingEntryResponse> userJoggingsQueriedSorted =
            userJoggingsQueried.stream().sorted(Comparator.comparing(JoggingEntryResponse::getLocalDateTime))
                .collect(Collectors.toList());

        List<JoggingEntryResponse> userJoggingsFilteredSorted =
            userJoggingsFiltered.stream().sorted(Comparator.comparing(JoggingEntryResponse::getLocalDateTime))
                .collect(Collectors.toList());

        for (int i = 0; i < userJoggingsQueriedSorted.size(); i++) {
            JoggingEntryResponse queried = userJoggingsQueriedSorted.get(i);
            JoggingEntryResponse filtered = userJoggingsFilteredSorted.get(i);

            assertThat(queried.getUserName()).isEqualTo(filtered.getUserName());
            assertThat(queried.getLocalDateTime()).isEqualTo(filtered.getLocalDateTime());
            assertThat(queried.getDistance()).isEqualTo(filtered.getDistance());

            LocationDto locationDtoQueried = queried.getLocationDto();
            LocationDto locationDtoFiltered = filtered.getLocationDto();
            TestUtility.assertDoublesReasonablyEqual(locationDtoQueried.getLatitude(),
                locationDtoFiltered.getLatitude());
            TestUtility.assertDoublesReasonablyEqual(locationDtoQueried.getLongitude(),
                locationDtoFiltered.getLongitude());
        }
    }
}
