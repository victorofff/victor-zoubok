package com.toptal.jogging.service;

import com.toptal.jogging.TestDatasourceConfig;
import com.toptal.jogging.domain.Role;
import com.toptal.jogging.domain.User;
import com.toptal.jogging.repository.UserRepository;
import com.toptal.jogging.service.impl.AuthenticationServiceImpl;
import com.toptal.jogging.service.impl.UserServiceImpl;
import com.toptal.jogging.util.TestConst;
import liquibase.exception.LiquibaseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    TestDatasourceConfig.class,
    UserServiceImpl.class,
    AuthenticationServiceImpl.class
})
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@MockBeans({
    @MockBean(PasswordEncoder.class)
})
@Transactional
public class AuthenticationServiceTest extends BaseTest {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserRepository userRepository;

    protected User rootUser;
    protected User managerUser;
    protected User standardUser;

    @Before
    public void setup() throws LiquibaseException {
        applyLatestChanges();

        standardUser = userRepository.findByUserName(TestConst.STANDARD_USER_NAME);
        assertThat(standardUser).isNotNull();
        assertThat(standardUser.getRoles().stream().map(Role::getRoleName)
            .filter(TestConst.STANDARD_USER_ROLE::equals).count()).isEqualTo(1);

        managerUser = userRepository.findByUserName(TestConst.MANAGER_USER_NAME);
        assertThat(managerUser).isNotNull();
        assertThat(managerUser.getRoles().stream().map(Role::getRoleName)
            .filter(TestConst.MANAGER_USER_ROLE::equals).count()).isEqualTo(1);

        rootUser = userRepository.findByUserName(TestConst.ADMIN_USER_NAME);
        assertThat(rootUser).isNotNull();
        assertThat(rootUser.getRoles().stream().map(Role::getRoleName)
            .filter(TestConst.ADMIN_USER_ROLE::equals).count()).isEqualTo(1);


    }

    @Test
    @WithMockUser(username = TestConst.STANDARD_USER_NAME, authorities = {"CRUD_OWN_USERS"})
    public void testStandardUser() {
        assertThat(authenticationService.hasPrivilege("CRUD_OWN_USERS")).isTrue();
        assertThat(authenticationService.getUser()).isEqualToComparingFieldByField(standardUser);
    }

    @Test
    @WithMockUser(username = TestConst.MANAGER_USER_NAME, authorities = {"CRUD_ALL_USERS"})
    public void testManagerUser() {
        assertThat(authenticationService.hasPrivilege("CRUD_ALL_USERS")).isTrue();
        assertThat(authenticationService.getUser()).isEqualToComparingFieldByField(managerUser);
    }

    @Test
    @WithMockUser(username = TestConst.ADMIN_USER_NAME, authorities = {"CRUD_ALL_RECORDS"})
    public void testRootUser() {
        assertThat(authenticationService.hasPrivilege("CRUD_ALL_RECORDS")).isTrue();
        assertThat(authenticationService.getUser()).isEqualToComparingFieldByField(rootUser);
    }

    @Test(expected = UsernameNotFoundException.class)
    @WithMockUser(username = "dummy.user", authorities = {"CRUD_ALL_RECORDS"})
    public void testNotValidUser() {
        authenticationService.getUser();
    }

    @Test
    @WithMockUser(username = TestConst.ADMIN_USER_NAME, authorities = {"DUMMY_AUTHORITY"})
    public void testNotValidAuthority() {
        // we assume there are valid authorities here so don't check at this level
        assertThat(authenticationService.hasPrivilege("DUMMY_AUTHORITY")).isTrue();
    }

}