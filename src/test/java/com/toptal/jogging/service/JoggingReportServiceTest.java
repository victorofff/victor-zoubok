package com.toptal.jogging.service;

import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.toptal.jogging.domain.Jogging;
import com.toptal.jogging.repository.JoggingRepository;
import com.toptal.jogging.response.JoggingAggregatedWeekResponse;
import com.toptal.jogging.util.TestUtility;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

public class JoggingReportServiceTest extends BaseServiceTest {

    @Getter
    @Setter
    @Builder
    private static class JoggingAggregatedWeekResponseDto {
        String userName;
        int year;
        int weekNumber;
        double averageSpeedKmHour;
        double averageMeters;
    }

    @Autowired
    private JoggingReportService joggingReportService;

    @Autowired
    private JoggingRepository joggingRepository;

    @Test
    public void testCreateWeeklyReportOneUser() {
        setupMockedStandardUser();

        List<JoggingAggregatedWeekResponse> report = joggingReportService.createWeeklyReport();

        assertThat(
            report.stream().map(JoggingAggregatedWeekResponse::getUserName).distinct()
                .collect(Collectors.toList()))
            .containsExactly(standardUser.getUserName());
    }

    @Test
    public void testCreateWeeklyReportPagingOneUser() {
        setupMockedStandardUser();

        assertThat(
            getReportPagedContent().stream().map(JoggingAggregatedWeekResponse::getUserName)
                .distinct()
                .collect(Collectors.toList()))
            .containsExactly(standardUser.getUserName());
    }

    @Test
    public void testCreateWeeklyReportPagingAllUsers() {
        setupMockedManagerUser();

        Set<String> reportUsers = getReportPagedContent().stream()
            .map(JoggingAggregatedWeekResponse::getUserName)
            .collect(Collectors.toSet());

        assertThat(reportUsers).containsExactlyInAnyOrder(
            joggingRepository.findAll().stream().map(jogging -> jogging.getUser().getUserName())
                .distinct()
                .toArray(String[]::new));
    }

    @Test
    public void testCreateWeeklyReportAllUsers() {
        setupMockedManagerUser();

        List<JoggingAggregatedWeekResponse> report = joggingReportService.createWeeklyReport();
        Set<String> reportUsers = report.stream().map(JoggingAggregatedWeekResponse::getUserName)
            .collect(Collectors.toSet());

        assertThat(reportUsers).containsExactlyInAnyOrder(
            joggingRepository.findAll().stream().map(jogging -> jogging.getUser().getUserName())
                .distinct()
                .toArray(String[]::new));
    }

    @Test
    public void testCheckReportLogicOneUser() {
        setupMockedStandardUser();

        List<JoggingAggregatedWeekResponseDto> joggings = joggingRepository.findAll().stream()
            .filter(jogging -> standardUser.getUserName().equals(jogging.getUser().getUserName()))
            .sorted(Comparator.comparing(Jogging::getTimeStamp))
            .map(jogging -> JoggingAggregatedWeekResponseDto.builder()
                .year(jogging.getTimeStamp().toLocalDateTime().getYear())
                .weekNumber(
                    jogging.getTimeStamp().toLocalDateTime().get(IsoFields.WEEK_OF_WEEK_BASED_YEAR))
                .averageMeters(jogging.getDistance())
                .averageSpeedKmHour(0)
                .userName(jogging.getUser().getUserName())
                .build()).collect(Collectors.toList());

        Map<Integer, Map<Integer, List<JoggingAggregatedWeekResponseDto>>> averages =
            joggings.stream()
                .collect(Collectors.groupingBy(JoggingAggregatedWeekResponseDto::getYear,
                    Collectors.groupingBy(JoggingAggregatedWeekResponseDto::getWeekNumber)));

        List<JoggingAggregatedWeekResponseDto> calculatedResults = new ArrayList<>();
        for (Map.Entry<Integer, Map<Integer, List<JoggingAggregatedWeekResponseDto>>> yearData : averages
            .entrySet()) {
            for (Map.Entry<Integer, List<JoggingAggregatedWeekResponseDto>> weekData : yearData
                .getValue().entrySet()) {
                JoggingAggregatedWeekResponseDto aggregated = JoggingAggregatedWeekResponseDto
                    .builder()
                    .year(yearData.getKey())
                    .weekNumber(weekData.getKey())
                    .userName(standardUser.getUserName())
                    .build();

                double averageMeters =
                    weekData.getValue().stream()
                        .mapToDouble(JoggingAggregatedWeekResponseDto::getAverageMeters).average()
                        .orElse(-1.0);
                assertThat(averageMeters).isGreaterThan(0.0);
                aggregated.setAverageMeters(averageMeters);
                aggregated.setAverageSpeedKmHour(averageMeters / (1000.0 * 7 * 24));

                calculatedResults.add(aggregated);
            }

        }

        List<JoggingAggregatedWeekResponse> reportData = joggingReportService.createWeeklyReport();
        assertThat(reportData.size()).isGreaterThan(0);
        assertThat(calculatedResults.size()).isEqualTo(reportData.size());
        for (int i = 0; i < reportData.size(); i++) {
            JoggingAggregatedWeekResponse reportResult = reportData.get(i);
            JoggingAggregatedWeekResponseDto calculatedResult = calculatedResults.get(i);

            assertThat(reportResult.getUserName()).isEqualTo(calculatedResult.getUserName());
            assertThat(reportResult.getYear()).isEqualTo(calculatedResult.getYear());
            assertThat(reportResult.getWeekNumber()).isEqualTo(calculatedResult.getWeekNumber());

            TestUtility.assertDoublesReasonablyEqual(reportResult.getAverageMeters(),
                calculatedResult.getAverageMeters());
            TestUtility.assertDoublesReasonablyEqual(reportResult.getAverageSpeedKmHour(),
                calculatedResult.getAverageSpeedKmHour());
        }
    }


    private List<JoggingAggregatedWeekResponse> getReportPagedContent() {
        List<JoggingAggregatedWeekResponse> result = new ArrayList<>();
        Page<JoggingAggregatedWeekResponse> page;
        int idx = 0;
        do {
            page = joggingReportService.createWeeklyReport(PageRequest.of(idx++, 2));
            result.addAll(page.toList());
        } while (page.hasContent());

        return result;
    }


}