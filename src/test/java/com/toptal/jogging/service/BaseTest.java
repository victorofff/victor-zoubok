package com.toptal.jogging.service;

import javax.sql.DataSource;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.TestPropertySource;


@TestPropertySource(locations = "classpath:application-test.properties")
@Slf4j
@EnableConfigurationProperties(LiquibaseProperties.class)
public abstract class BaseTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private LiquibaseProperties properties;

    @Autowired
    private ResourceLoader resourceLoader;

    protected void applyLatestChanges() throws LiquibaseException {
        log.info("Applying latest liquibase changes...");
        SpringLiquibase liquibase = getSpringLiquibase();
        liquibase.afterPropertiesSet();
        log.info("Database up to date.");
    }

    public SpringLiquibase getSpringLiquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:/db/liquibase-changelog.xml");
        liquibase.setContexts(properties.getContexts());
        liquibase.setDefaultSchema(properties.getDefaultSchema());
        liquibase.setDropFirst(properties.isDropFirst());
        liquibase.setShouldRun(properties.isEnabled());
        liquibase.setLabels(properties.getLabels());
        liquibase.setChangeLogParameters(properties.getParameters());
        liquibase.setRollbackFile(properties.getRollbackFile());
        liquibase.setResourceLoader(resourceLoader);
        return liquibase;
    }
}
