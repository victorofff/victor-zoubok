package com.toptal.jogging.util;

public class TestConst {
    public static final String STANDARD_USER_NAME = "john.doe";
    public static final String ADMIN_USER_NAME = "bob.frapples";
    public static final String MANAGER_USER_NAME = "bill.robson";

    public static final String STANDARD_USER_ROLE = "STANDARD_USER";
    public static final String ADMIN_USER_ROLE = "ADMIN_USER";
    public static final String MANAGER_USER_ROLE = "MANAGER_USER";

    public static final String STANDARD_USER_PASSWORD = "jwtpass";
    public static final String ADMIN_USER_PASSWORD = "jwtpass";
    public static final String MANAGER_USER_PASSWORD = "jwtpass";

    public static final long JOGGING_ID_STANDARD_USER = 1;
    public static final long JOGGING_ID_ANOTHER_STANDARD_USER = 2;
    public static final long JOGGING_ID_MANAGER_USER = 10;
    public static final long JOGGING_ID_INVALID = -1;

    public static final String OAUTH_TOKEN_ENDPOINT = "/oauth/token";

}
