package com.toptal.jogging.util;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;

import com.toptal.jogging.response.JoggingEntryResponse;
import com.toptal.jogging.service.JoggingService;
import liquibase.integration.spring.SpringLiquibase;
import org.assertj.core.data.Percentage;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public class TestUtility {

    private static final double EQUALITY_PERCENTAGE = 0.00001;

    private TestUtility() {
    }

    public static void assertDoublesReasonablyEqual(double actual, double expected) {
        assertThat(actual)
            .withFailMessage("Two doubles are different more than: " + EQUALITY_PERCENTAGE)
            .isCloseTo(expected, Percentage.withPercentage(EQUALITY_PERCENTAGE));
    }

    public static List<JoggingEntryResponse> getJoggingPagedContent(JoggingService joggingService, String filter) {
        List<JoggingEntryResponse> result = new ArrayList<>();
        Page<JoggingEntryResponse> page;
        int idx = 0;
        do {
            page = joggingService.findAllPageable(filter, PageRequest.of(idx++, 2));
            result.addAll(page.toList());
        } while (page.hasContent());

        return result;
    }

}
