package com.toptal.jogging.controller;

import com.toptal.jogging.TestAppConfig;
import com.toptal.jogging.TestControllerConfig;
import com.toptal.jogging.TestDatasourceConfig;
import com.toptal.jogging.config.AuthorizationServerConfig;
import com.toptal.jogging.config.ResourceServerConfig;
import com.toptal.jogging.config.SecurityConfig;
import com.toptal.jogging.criteria.impl.CriteriaParserImpl;
import com.toptal.jogging.error.RestExceptionHandler;
import com.toptal.jogging.repository.UserRepository;
import com.toptal.jogging.service.BaseTest;
import com.toptal.jogging.service.impl.ApplicationUserDetailsServiceImpl;
import com.toptal.jogging.service.impl.AuthenticationServiceImpl;
import com.toptal.jogging.service.impl.JoggingReportServiceImpl;
import com.toptal.jogging.service.impl.JoggingServiceImpl;
import com.toptal.jogging.service.impl.UserServiceImpl;
import com.toptal.jogging.service.impl.WeatherServiceImpl;
import com.toptal.jogging.util.TestConst;
import liquibase.exception.LiquibaseException;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.cloud.contract.spec.internal.HttpStatus;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(
    classes = {
        TestDatasourceConfig.class,
        UserServiceImpl.class,
        JoggingReportServiceImpl.class,
        WeatherServiceImpl.class,
        CriteriaParserImpl.class,
        JoggingServiceImpl.class,
        UsersController.class,
        JoggingController.class,
        JoggingReportController.class,
        SecurityConfig.class,
        ResourceServerConfig.class,
        AuthorizationServerConfig.class,
        ApplicationUserDetailsServiceImpl.class,
        AuthenticationServiceImpl.class,
        TestControllerConfig.class,
        TestAppConfig.class,
        RestExceptionHandler.class,
    })
@MockBeans({
    @MockBean(RestTemplate.class)
})
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@Transactional
public abstract class ControllerBaseTest extends BaseTest {

    private static final String GRANT_TYPE = "grant_type";
    private static final String CLIENT_ID = "client_id";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String GRANT_TYPE_PASSWORD = "password";
    private static final String ATTRIBUTE_ACCESS_TOKEN = "access_token";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Value("${security.jwt.client-id:joggingapiclientid}")
    protected String clientId;

    @Value("${security.jwt.client-secret:XY7kmzoNzl100}")
    protected String clientSecret;

    protected MockMvc mockMvc;

    @Before
    public void setup() throws LiquibaseException {
        applyLatestChanges();

        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
            .addFilter(springSecurityFilterChain)
            .build();

    }

    protected String obtainAccessToken(String username, String password) throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add(GRANT_TYPE, GRANT_TYPE_PASSWORD);
        params.add(CLIENT_ID, clientId);
        params.add(USERNAME, username);
        params.add(PASSWORD, password);

        MvcResult result = mockMvc
            .perform(
                post(TestConst.OAUTH_TOKEN_ENDPOINT)
                    .params(params)
                    .with(httpBasic(clientId, clientSecret))
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andReturn();

        if (result.getResponse().getStatus() != HttpStatus.OK) {
            throw new IllegalArgumentException("Invalid JWT token");
        }

        String resultString = result.getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get(ATTRIBUTE_ACCESS_TOKEN).toString();
    }

}
