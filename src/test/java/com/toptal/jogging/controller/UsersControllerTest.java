package com.toptal.jogging.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.toptal.jogging.request.UserRequest;
import com.toptal.jogging.util.TestConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UsersControllerTest extends ControllerBaseTest {

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testNoTokenUnauthorized() throws Exception {
        mockMvc
            .perform(get("/account/users"))
            .andDo(print())
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.apierror.status", is("UNAUTHORIZED")))
            .andExpect(jsonPath("$.apierror.debugMessage",
                is("AuthenticationCredentialsNotFoundException: An Authentication object was not found in the "
                    + "SecurityContext")));
    }

    @Test
    public void tesAuthorizedAsRootUser() throws Exception {
        String accessToken = obtainAccessToken(TestConst.ADMIN_USER_NAME, TestConst.ADMIN_USER_PASSWORD);
        mockMvc.perform(get("/account/users")
            .header("Authorization", "Bearer " + accessToken))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].userName").isArray())
            .andExpect(jsonPath("length($.[*].id)", is(3)))
            .andExpect(jsonPath("$.[*].userName").isArray())
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.ADMIN_USER_NAME)))
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.STANDARD_USER_NAME)))
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.MANAGER_USER_NAME)));
    }

    @Test
    public void tesNonAuthorizedAsManagerUser() throws Exception {
        String accessToken = obtainAccessToken(TestConst.MANAGER_USER_NAME, TestConst.MANAGER_USER_PASSWORD);

        mockMvc.perform(get("/account/users")
            .header("Authorization", "Bearer " + accessToken))
            .andDo(print())
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.apierror.status", is("UNAUTHORIZED")))
            //make sure @EnableGlobalMethodSecurity is triggered, not NonAuthorizedException at service level
            .andExpect(jsonPath("$.apierror.debugMessage", is("AccessDeniedException: Access is denied")));
    }

    @Test
    public void tesNonAuthorizedAsStandardUser() throws Exception {
        String accessToken = obtainAccessToken(TestConst.STANDARD_USER_NAME, TestConst.STANDARD_USER_PASSWORD);

        mockMvc.perform(get("/account/users")
            .header("Authorization", "Bearer " + accessToken))
            .andDo(print())
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.apierror.status", is("UNAUTHORIZED")))
            //make sure @EnableGlobalMethodSecurity is triggered, not NonAuthorizedException at service level
            .andExpect(jsonPath("$.apierror.debugMessage", is("AccessDeniedException: Access is denied")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void tesNonAuthorizedInvalidCredentials() throws Exception {
        obtainAccessToken(TestConst.STANDARD_USER_NAME, "dummyPassword");
    }

    @Test(expected = IllegalArgumentException.class)
    public void tesNonAuthorizedInvalidUser() throws Exception {
        obtainAccessToken("dummyUser", "dummyPassword");
    }

    @Test(expected = IllegalArgumentException.class)
    public void tesNonAuthorizedInvalidClientId() throws Exception {
        clientId = "dummy";
        obtainAccessToken(TestConst.STANDARD_USER_NAME, TestConst.STANDARD_USER_PASSWORD);
    }

    @Test
    public void testInvalidCreateRequest() throws Exception {
        String adminAccessToken = obtainAccessToken(TestConst.ADMIN_USER_NAME, TestConst.ADMIN_USER_PASSWORD);

        UserRequest userRequest = UserRequest.builder()
            .firstName("Ryan")
            .lastName("   ")
            .userName("ryan.walsh")
            .password("password123")
            .build();

        userRequest.getRoles().add("STANDARD_USER");

        String userRequestContent = mapper.writeValueAsString(userRequest);
        mockMvc.perform(
            post("/account")
                .header("Authorization", "Bearer " + adminAccessToken)
                .content(userRequestContent)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.apierror.status", is("BAD_REQUEST")))
            .andExpect(jsonPath("$.apierror.message", is("Validation error")));
    }

}