package com.toptal.jogging.controller;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.toptal.jogging.dto.LocationDto;
import com.toptal.jogging.request.JoggingEntryRequest;
import com.toptal.jogging.request.UserRequest;
import com.toptal.jogging.util.TestConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ComplexScenarioTest extends ControllerBaseTest {

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testAdminUserCreatesNewStandardUserAccountAndGetReport() throws Exception {
        String adminAccessToken = obtainAccessToken(TestConst.ADMIN_USER_NAME, TestConst.ADMIN_USER_PASSWORD);

        UserRequest userRequest = UserRequest.builder()
            .firstName("Alice")
            .lastName("Cooper")
            .userName("alice.cooper")
            .password("admin123")
            .build();

        userRequest.getRoles().add("STANDARD_USER");

        String userRequestContent = mapper.writeValueAsString(userRequest);
        mockMvc.perform(
            post("/account")
                .header("Authorization", "Bearer " + adminAccessToken)
                .content(userRequestContent)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.userName", is("alice.cooper")))
            .andExpect(jsonPath("$.roles[0]", is("STANDARD_USER")));

        String standardUserToken = obtainAccessToken("alice.cooper", "admin123");
        JoggingEntryRequest joggingEntryRequest = JoggingEntryRequest.builder()
            .distance(1232)
            .localDateTime(LocalDateTime.now())
            .locationDto(new LocationDto(12.345, 14.478))
            .build();

        String joggingEntryRequestContent = mapper.writeValueAsString(joggingEntryRequest);
        mockMvc.perform(
            post("/jogging-crud")
                .header("Authorization", "Bearer " + standardUserToken)
                .content(joggingEntryRequestContent)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isCreated());

        mockMvc.perform(get("/jogging-report/joggings-weekly")
            .header("Authorization", "Bearer " + standardUserToken))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].userName").isArray())
            .andExpect(jsonPath("$.[*].userName", hasItem("alice.cooper")));
    }

}
