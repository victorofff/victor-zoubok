package com.toptal.jogging.controller;

import com.toptal.jogging.util.TestConst;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class JoggingReportControllerTest extends ControllerBaseTest {
    @Test
    public void testNoTokenUnauthorized() throws Exception {
        mockMvc
            .perform(get("/jogging-report/joggings-weekly"))
            .andDo(print())
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.apierror.status", is("UNAUTHORIZED")))
            .andExpect(jsonPath("$.apierror.debugMessage",
                is("AuthenticationCredentialsNotFoundException: An Authentication object was not found in the SecurityContext")));
    }

    @Test
    public void testAccessAllowedAsStandardUser() throws Exception {
        String accessToken = obtainAccessToken(TestConst.STANDARD_USER_NAME, TestConst.STANDARD_USER_PASSWORD);

        mockMvc.perform(get("/jogging-report/joggings-weekly")
            .header("Authorization", "Bearer " + accessToken))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].userName").isArray())
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.STANDARD_USER_NAME)))
            .andExpect(jsonPath("$.[*].userName", not(hasItem(TestConst.ADMIN_USER_NAME))))
            .andExpect(jsonPath("$.[*].userName", not(hasItem(TestConst.MANAGER_USER_NAME))));
    }

    @Test
    public void testAccessAllowedAsManagerUser() throws Exception {
        String accessToken = obtainAccessToken(TestConst.MANAGER_USER_NAME, TestConst.MANAGER_USER_PASSWORD);

        mockMvc.perform(get("/jogging-report/joggings-weekly")
            .header("Authorization", "Bearer " + accessToken))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].userName").isArray())
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.STANDARD_USER_NAME)))
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.MANAGER_USER_NAME)));
    }

    @Test
    public void testAccessAllowedAsRootUser() throws Exception {
        String accessToken = obtainAccessToken(TestConst.ADMIN_USER_NAME, TestConst.ADMIN_USER_PASSWORD);

        mockMvc.perform(get("/jogging-report/joggings-weekly")
            .header("Authorization", "Bearer " + accessToken))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].userName").isArray())
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.STANDARD_USER_NAME)))
            .andExpect(jsonPath("$.[*].userName", hasItem(TestConst.MANAGER_USER_NAME)));
    }
}
