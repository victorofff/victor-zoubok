package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should update an existing non-owned record with root user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 890,
                        locationDto: [
                                latitude : 8135.78,
                                longitude: 8156.456
                        ]
                )
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 890,
                        locationDto: [
                                latitude : 8135.78,
                                longitude: 8156.456
                        ],
                        weatherJson: null,
                        userName: "bob.frapples"
                )
            }
        }
]
