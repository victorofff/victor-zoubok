package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should create a new record with manager user"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status CREATED()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ],
                        weatherJson: null,
                        userName: "bill.robson"
                )
            }
        },

        Contract.make {
            description "should return bad request on invalid date format"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "XXX",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Malformed JSON request'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return validation error on invalid locationDto"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: null
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Validation error'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return validation error on negative distance"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: -376,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Validation error'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return validation error on null latitude or longitude"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 3766,
                        locationDto: [
                                latitude : null,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Validation error'), p(nonBlank())),
                        ]
                )
            }
        },
        Contract.make {
            description "should return validation error on null latitude or longitude"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 3766,
                        locationDto: [
                                latitude : null,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Validation error'), p(nonBlank())),
                        ]
                )
            }
        },
]