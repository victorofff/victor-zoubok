package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[

        Contract.make {
            description "should return the first page of jogging records with filtering criteria with standard user"
            request {
                method GET()
                url($(c('/api/jogging-crud/joggings-pageable'), p('/jogging-crud/joggings-pageable'))) {
                    queryParameters {
                        parameter("filter", "timeStamp eq '2020-01-09 01:52:46.269'")
                        parameter("page", "0")
                        parameter("size", "1")
                    }
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [
                                        [
                                                "id"              : 7,
                                                "localDateTime"   : "2020-01-09 01:52:46.269",
                                                "distance"        : 27,
                                                "locationDto": [
                                                        "latitude" : 1.4,
                                                        "longitude": 1.5
                                                ],
                                                "weatherJson"     : null,
                                                "userName"        : "john.doe"
                                        ]
                                ],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 0,
                                        "pageNumber": 0,
                                        "pageSize"  : 1,
                                        "unpaged"   : false,
                                        "paged"     : true
                                ],
                                "last"            : true,
                                "totalPages"      : 1,
                                "totalElements"   : 1,
                                "size"            : 1,
                                "number"          : 0,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 1,
                                "first"           : true,
                                "empty"           : false
                        ]
                )

            }
        },

        Contract.make {
            description "should return the empty page of non-owned jogging records with filtering criteria with standard user"
            request {
                method GET()
                url($(c('/api/jogging-crud/joggings-pageable'), p('/jogging-crud/joggings-pageable'))) {
                    queryParameters {
                        parameter("filter", "timeStamp eq '2018-01-16 01:52:46.269'")
                        parameter("page", "0")
                        parameter("size", "1")
                    }
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 0,
                                        "pageSize"  : 1,
                                        "pageNumber": 0,
                                        "unpaged"   : false,
                                        "paged"     : true
                                ],
                                "totalPages"      : 0,
                                "last"            : true,
                                "totalElements"   : 0,
                                "size"            : 1,
                                "number"          : 0,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 0,
                                "first"           : true,
                                "empty"           : true
                        ]
                )

            }
        }
]