package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should create a new record with standard user"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status CREATED()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ],
                        weatherJson: null,
                        userName: "john.doe"
                )
            }
        },

        Contract.make {
            description "should return conflict on existing record"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ],
                )
            }
            response {
                status CONFLICT()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('CONFLICT'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('dbcSQLIntegrityConstraintViolationException: Unique index or primary key violation'), p(nonBlank())),
                        ]
                )
            }
        },
]