package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should return a non-owned jogging record"
            request {
                method GET()
                urlPath $(c('/api/jogging-crud/jogging/1'), p('/jogging-crud/jogging/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "id"              : 1,
                                "localDateTime"   : "2020-02-08 01:52:46.269",
                                "distance"        : 3,
                                "locationDto": [
                                        "latitude" : 0.1,
                                        "longitude": 0.2
                                ],
                                "weatherJson"     : null,
                                "userName"        : "john.doe"
                        ]
                )
            }
        },
        Contract.make {
            description "should return an owned jogging record"
            request {
                method GET()
                urlPath $(c('/api/jogging-crud/jogging/11'), p('/jogging-crud/jogging/11'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "id"              : 11,
                                "localDateTime"   : "2018-01-13 01:52:46.269",
                                "distance"        : 2,
                                "locationDto": [
                                        "latitude" : 2.2,
                                        "longitude": 2.3
                                ],
                                "weatherJson"     : null,
                                "userName"        : "bill.robson"
                        ]
                )
            }
        },

]