package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should return an owned jogging record with standard user"
            request {
                method GET()
                urlPath $(c('/api/jogging-crud/jogging/1'), p('/jogging-crud/jogging/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "id"              : 1,
                                "localDateTime": "2020-02-08 01:52:46.269",
                                "distance"        : 3,
                                "locationDto": [
                                    "latitude" : 0.1 ,
                                    "longitude" : 0.2
                                ],
                                "weatherJson"     : null,
                                "userName"        : "john.doe"
                        ]
                )
            }
        }
]