package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should update an existing owned record with standard user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 8910,
                        locationDto: [
                                latitude : 8135.786,
                                longitude: 8156.4566
                        ]
                )
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 8910,
                        locationDto: [
                                latitude : 8135.786,
                                longitude: 8156.4566
                        ],
                        weatherJson: null,
                        userName: "john.doe"
                )
            }
        },

        Contract.make {
            description "should return not found on non-owned record with standard user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/13'), p('/jogging-crud/13'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2018-01-15 01:52:46.269",
                        distance: 8902,
                        locationDto: [
                                latitude : 811355.78,
                                longitude: 811561.456
                        ]
                )
            }
            response {
                status NOT_FOUND()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('NOT_FOUND'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Entity com.toptal.jogging.domain.Jogging with id 13 not found'), p(nonBlank())),
                        ]
                )
            }
        },
]