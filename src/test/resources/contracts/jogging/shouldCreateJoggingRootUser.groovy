package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should create a new record with root user"
            request {
                method POST()
                urlPath $(c('/api/jogging-crud'), p('/jogging-crud'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status CREATED()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2015-02-08 01:52:46.345",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ],
                        weatherJson: null,
                        userName: "bob.frapples"
                )
            }
        },
]