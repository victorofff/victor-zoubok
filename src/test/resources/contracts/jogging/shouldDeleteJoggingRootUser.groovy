package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should delete an existing non-owned record with root user"
            request {
                method DELETE()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
            }
        }
]
