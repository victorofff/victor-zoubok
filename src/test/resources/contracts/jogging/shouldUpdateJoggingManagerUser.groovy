package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should update an existing non-owned record with manager user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 890,
                        locationDto: [
                                latitude : 8135.78,
                                longitude: 8156.456
                        ]
                )
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 890,
                        locationDto: [
                                latitude : 8135.78,
                                longitude: 8156.456
                        ],
                        weatherJson: null,
                        userName: "bill.robson"
                )
            }
        },

        Contract.make {
            description "should update an existing owned record with manager user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/13'), p('/jogging-crud/13'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2018-01-15 01:52:46.269",
                        distance: 890,
                        locationDto: [
                                latitude : 81355.78,
                                longitude: 81561.456
                        ]
                )
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: $(c(156), p(positiveInt())),
                        localDateTime: "2018-01-15 01:52:46.269",
                        distance: 890,
                        locationDto: [
                                latitude : 81355.78,
                                longitude: 81561.456
                        ],
                        weatherJson: null,
                        userName: "bill.robson"
                )
            }
        },

        Contract.make {
            description "should return not found on missing non-owned jogging record with manager user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/100'), p('/jogging-crud/100'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2018-01-15 01:52:46.269",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status NOT_FOUND()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('NOT_FOUND'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Entity com.toptal.jogging.domain.Jogging with id 100 not found'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return bad request on invalid date format and an existing non-owned record with manager user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "XXX",
                        distance: 345,
                        locationDto: [
                                latitude : 135.78,
                                longitude: 156.456
                        ]
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Malformed JSON request'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return bad request on invalid locationDto and an existing non-owned record with manager user"
            request {
                method PUT()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        localDateTime: "2020-02-08 01:52:46.269",
                        distance: 34554,
                        locationDto: null
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Validation error'), p(nonBlank())),
                        ]
                )
            }
        }
]