package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should return the first page of jogging records with filtering criteria with root user"
            request {
                method GET()
                url($(c('/api/jogging-crud/joggings-pageable'), p('/jogging-crud/joggings-pageable'))) {
                    queryParameters {
                        parameter("filter", "timeStamp eq '2020-01-08 01:52:46.269'")
                        parameter("page", "0")
                        parameter("size", "1")
                    }
                }
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [
                                        [
                                                "id"              : 6,
                                                "localDateTime"   : "2020-01-08 01:52:46.269",
                                                "distance"        : 12,
                                                "locationDto": [
                                                        "latitude" : 1.2,
                                                        "longitude": 1.3
                                                ],
                                                "weatherJson"     : null,
                                                "userName"        : "john.doe"
                                        ]
                                ],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 0,
                                        "pageNumber": 0,
                                        "pageSize"  : 1,
                                        "paged"     : true,
                                        "unpaged"   : false
                                ],
                                "totalElements"   : 1,
                                "totalPages"      : 1,
                                "last"            : true,
                                "size"            : 1,
                                "number"          : 0,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 1,
                                "first"           : true,
                                "empty"           : false
                        ]
                )
            }
        }
]