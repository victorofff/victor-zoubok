package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should delete an existing non-owned record with manager user"
            request {
                method DELETE()
                urlPath $(c('/api/jogging-crud/1'), p('/jogging-crud/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
            }
        },

        Contract.make {
            description "should delete an existing owned record with manager user"
            request {
                method DELETE()
                urlPath $(c('/api/jogging-crud/13'), p('/jogging-crud/13'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
            }
        },

        Contract.make {
            description "should return not found on missing non-owned jogging record with manager user"
            request {
                method DELETE()
                urlPath $(c('/api/jogging-crud/100'), p('/jogging-crud/100'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status NOT_FOUND()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('NOT_FOUND'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Entity com.toptal.jogging.domain.Jogging with id 100 not found'), p(nonBlank())),
                        ]
                )
            }
        },

]