package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should receive a page from jogging report with root user"
            request {
                method GET()
                url($(c('/api/jogging-report/joggings-weekly-pageable'), p('/jogging-report/joggings-weekly-pageable'))) {
                    queryParameters {
                        parameter("page", "0")
                        parameter("size", "1")
                    }
                }
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [
                                        [
                                                "year"              : 2017,
                                                "weekNumber"        : 3,
                                                "userName"          : "bill.robson",
                                                "averageMeters"     : 4.5,
                                                "averageSpeedKmHour": 0.000027
                                        ]
                                ],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 0,
                                        "pageNumber": 0,
                                        "pageSize"  : 1,
                                        "paged"     : true,
                                        "unpaged"   : false
                                ],
                                "totalPages"      : 7,
                                "totalElements"   : 7,
                                "last"            : false,
                                "size"            : 1,
                                "number"          : 0,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 1,
                                "first"           : true,
                                "empty"           : false
                        ]
                )

            }
        }
]