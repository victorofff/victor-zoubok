package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[

        Contract.make {
            description "should receive jogging report with standard user"
            request {
                method GET()
                urlPath($(c('/api/jogging-report/joggings-weekly'), p('/jogging-report/joggings-weekly')))
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        '''
[
  {
    "year": 2020,
    "userName": "john.doe",
    "weekNumber": 2,
    "averageMeters": 14.75,
    "averageSpeedKmHour": 0.000088
  },
  {
    "year": 2020,
    "userName": "john.doe",
    "weekNumber": 6,
    "averageMeters": 3.5,
    "averageSpeedKmHour": 0.000021
  },
  {
    "year": 2020,
    "userName": "john.doe",
    "weekNumber": 7,
    "averageMeters": 11.666667,
    "averageSpeedKmHour": 0.000069
  }
]
'''
                )
            }
        },
]