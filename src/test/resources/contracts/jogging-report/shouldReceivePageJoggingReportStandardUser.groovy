package contracts.jogging

import org.springframework.cloud.contract.spec.Contract

[

        Contract.make {
            description "should receive a page from jogging report with standard user"
            request {
                method GET()
                url($(c('/api/jogging-report/joggings-weekly-pageable'), p('/jogging-report/joggings-weekly-pageable'))) {
                    queryParameters {
                        parameter("page", "0")
                        parameter("size", "1")
                    }
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [
                                        [
                                                "year"              : 2020,
                                                "weekNumber"        : 2,
                                                "userName"          : "john.doe",
                                                "averageSpeedKmHour": 0.000088,
                                                "averageMeters"     : 14.75
                                        ]
                                ],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 0,
                                        "pageNumber": 0,
                                        "pageSize"  : 1,
                                        "unpaged"   : false,
                                        "paged"     : true
                                ],
                                "totalElements"   : 3,
                                "last"            : false,
                                "totalPages"      : 3,
                                "size"            : 1,
                                "number"          : 0,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 1,
                                "first"           : true,
                                "empty"           : false
                        ]
                )
            }
        },
]