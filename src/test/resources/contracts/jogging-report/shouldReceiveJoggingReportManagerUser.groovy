package contracts.jogging


import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should receive jogging report with manager user"
            request {
                method GET()
                urlPath($(c('/api/jogging-report/joggings-weekly'), p('/jogging-report/joggings-weekly')))
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        '''
[
  {
    "year": 2017,
    "weekNumber": 3,
    "userName": "bill.robson",
    "averageMeters": 4.5,
    "averageSpeedKmHour": 0.000027
  },
  {
    "year": 2018,
    "weekNumber": 2,
    "userName": "bill.robson",
    "averageMeters": 2,
    "averageSpeedKmHour": 0.000012
  },
  {
    "year": 2018,
    "weekNumber": 3,
    "userName": "bill.robson",
    "averageMeters": 4.666667,
    "averageSpeedKmHour": 0.000028
  },
  {
    "year": 2020,
    "weekNumber": 2,
    "userName": "bill.robson",
    "averageMeters": 27,
    "averageSpeedKmHour": 0.000161
  },
  {
    "year": 2020,
    "weekNumber": 2,
    "userName": "john.doe",
    "averageMeters": 14.75,
    "averageSpeedKmHour": 0.000088
  },
  {
    "year": 2020,
    "weekNumber": 6,
    "userName": "john.doe",
    "averageMeters": 3.5,
    "averageSpeedKmHour": 0.000021
  },
  {
    "year": 2020,
    "weekNumber": 7,
    "userName": "john.doe",
    "averageMeters": 11.666667,
    "averageSpeedKmHour": 0.000069
  }
]
'''
                )

            }
        },
]