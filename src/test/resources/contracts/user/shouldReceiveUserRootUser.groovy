package contracts.user

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should return a specific user"
            request {
                method GET()
                urlPath $(c('/api/account/user/1'), p('/account/user/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        id: 1,
                        userName: "john.doe",
                        firstName: "John",
                        lastName: "Doe",
                        roles: ["STANDARD_USER"]
                )
            }
        },
        Contract.make {
            description "should return not found on missing user"
            request {
                method GET()
                urlPath $(c('/api/account/user/10'), p('/account/user/10'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status NOT_FOUND()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('NOT_FOUND'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Entity com.toptal.jogging.domain.User with id 10 not found'), p(nonBlank())),
                        ]
                )
            }
        }
]