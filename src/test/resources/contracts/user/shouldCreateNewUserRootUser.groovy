package contracts.user

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should create a new user"
            request {
                method POST()
                urlPath $(c('/api/account'), p('/account'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        userName: "alice.cooper",
                        firstName: "Alice",
                        lastName: "Cooper",
                        password: "xxxx",
                        roles: ["STANDARD_USER", "MANAGER_USER"],
                )
            }
            response {
                status CREATED()
                headers {
                    contentType(applicationJson())
                }
                body(
                        userName: "alice.cooper",
                        firstName: "Alice",
                        lastName: "Cooper",
                        roles: ["STANDARD_USER", "MANAGER_USER"]
                )
            }
        },

        Contract.make {
            description "should return not found on invalid role"
            request {
                method POST()
                urlPath $(c('/api/account'), p('/account'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        userName: "alice.cooper",
                        firstName: "Alice",
                        lastName: "Cooper",
                        password: "xxxx",
                        roles: ["STANDARD_USER", "MISSING_ROLE"]
                )
            }
            response {
                status NOT_FOUND()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('NOT_FOUND'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Entity com.toptal.jogging.domain.Role with business id MISSING_ROLE not found'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return conflict on existing user"
            request {
                method POST()
                urlPath $(c('/api/account'), p('/account'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        userName: "john.doe",
                        firstName: "John",
                        lastName: "Doe",
                        password: "xxxxxx",
                        roles: ["STANDARD_USER", "MANAGER_USER", "ADMIN_USER"],
                )
            }
            response {
                status CONFLICT()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('CONFLICT'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('dbcSQLIntegrityConstraintViolationException: Unique index or primary key violation'), p(nonBlank())),
                        ]
                )
            }
        },

        Contract.make {
            description "should return validation error on blank user firstName"
            request {
                method POST()
                urlPath $(c('/api/account'), p('/account'))
                headers {
                    contentType(applicationJson())
                }
                body(
                        userName: "ryan.walsh",
                        firstName: "  ",
                        lastName: "Walsh",
                        password: "xxxx",
                        roles: ["STANDARD_USER", "MANAGER_USER"],
                )
            }
            response {
                status BAD_REQUEST()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('BAD_REQUEST'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c('Validation error'), p(nonBlank())),
                        ]
                )
            }
        }
]