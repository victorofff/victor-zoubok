package contracts.user

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should receive a page with users"
            request {
                method GET()
                url ($(c('/api/users-pageable'), p('/account/users-pageable'))) {
                    queryParameters {
                        parameter("page", "0")
                        parameter("size", "1")
                    }
                }
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [
                                        [
                                                "id"       : 1,
                                                "userName" : "john.doe",
                                                "firstName": "John",
                                                "lastName" : "Doe",
                                                "roles"    : [
                                                        "STANDARD_USER"
                                                ]
                                        ]
                                ],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 0,
                                        "pageNumber": 0,
                                        "pageSize"  : 1,
                                        "paged"     : true,
                                        "unpaged"   : false
                                ],
                                "last"            : false,
                                "totalElements"   : 3,
                                "totalPages"      : 3,
                                "size"            : 1,
                                "number"          : 0,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 1,
                                "first"           : true,
                                "empty"           : false
                        ]
                )
            }
        },

        Contract.make {
            description "should receive a an empty page if the paging criteria is out of bounds"
            request {
                method GET()
                url ($(c('/api/users-pageable'), p('/account/users-pageable'))) {
                    queryParameters {
                        parameter("page", "100")
                        parameter("size", "1")
                    }
                }
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                "content"         : [],
                                "pageable"        : [
                                        "sort"      : [
                                                "sorted"  : false,
                                                "unsorted": true,
                                                "empty"   : true
                                        ],
                                        "offset"    : 100,
                                        "pageSize"  : 1,
                                        "pageNumber": 100,
                                        "unpaged"   : false,
                                        "paged"     : true
                                ],
                                "totalElements"   : 3,
                                "last"            : true,
                                "totalPages"      : 3,
                                "size"            : 1,
                                "number"          : 100,
                                "sort"            : [
                                        "sorted"  : false,
                                        "unsorted": true,
                                        "empty"   : true
                                ],
                                "numberOfElements": 0,
                                "first"           : false,
                                "empty"           : true
                        ]
                )

            }
        }
]