package contracts.user

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should return unauthorized on receiving non-accessible user"
            request {
                method GET()
                urlPath $(c('/api/account/user/1'), p('/account/user/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status UNAUTHORIZED()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('UNAUTHORIZED'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c("Access allowed only for user owing the privilege 'CRUD_ALL_RECORDS'"), p(nonBlank())),
                        ]
                )
            }
        },
        Contract.make {
            description "should return unauthorized on deleting non-accessible user"
            request {
                method DELETE()
                urlPath $(c('/api/account/1'), p('/account/1'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status UNAUTHORIZED()
                headers {
                    contentType(applicationJson())
                }
                body(
                        apierror: [
                                status   : $('UNAUTHORIZED'),
                                timestamp: $(c('2020-02-14 11:20:35.348'), p(regex('([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.\\d{3})'))),
                                message  : $(c("Access allowed only for user owing the privilege 'CRUD_ALL_RECORDS'"), p(nonBlank())),
                        ]
                )
            }
        }
]