package contracts.user

import org.springframework.cloud.contract.spec.Contract

[
        Contract.make {
            description "should return all users"
            request {
                method GET()
                urlPath $(c('/api/users'), p('/account/users'))
                headers {
                    contentType(applicationJson())
                }
            }
            response {
                status OK()
                headers {
                    contentType(applicationJson())
                }
                body(
                        [
                                [
                                        "id"       : 1,
                                        "userName" : "john.doe",
                                        "firstName": "John",
                                        "lastName" : "Doe",
                                        "roles"    : [
                                                "STANDARD_USER"
                                        ]
                                ],
                                [
                                        "id"       : 2,
                                        "userName" : "bob.frapples",
                                        "firstName": "Bob",
                                        "lastName" : "Frapples",
                                        "roles"    : [
                                                "ADMIN_USER"
                                        ]
                                ],
                                [
                                        "id"       : 3,
                                        "userName" : "bill.robson",
                                        "firstName": "Bill",
                                        "lastName" : "Robson",
                                        "roles"    : [
                                                "MANAGER_USER"
                                        ]
                                ]
                        ]
                )
            }
        }
]