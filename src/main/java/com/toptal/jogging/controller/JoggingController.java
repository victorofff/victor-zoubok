package com.toptal.jogging.controller;

import java.util.List;

import com.toptal.jogging.request.JoggingEntryRequest;
import com.toptal.jogging.response.JoggingEntryResponse;
import com.toptal.jogging.service.JoggingService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/jogging-crud")
@RequiredArgsConstructor
public class JoggingController {

    private final JoggingService joggingService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public JoggingEntryResponse create(
        @Validated @RequestBody JoggingEntryRequest joggingEntryRequest) {
        return joggingService.save(joggingEntryRequest);
    }

    @PutMapping("/{id}")
    public JoggingEntryResponse update(@PathVariable long id,
        @Validated @RequestBody JoggingEntryRequest userRequest) {
        return joggingService.update(id, userRequest);
    }

    @GetMapping("/joggings")
    public List<JoggingEntryResponse> findAll(@RequestParam(value = "filter", required = false) String filter) {
        return joggingService.findAll(filter);
    }

    @GetMapping("/joggings-pageable")
    public Page<JoggingEntryResponse> findAllPageable(@RequestParam(value = "filter", required = false) String filter,
        Pageable pageable) {
        return joggingService.findAllPageable(filter, pageable);
    }

    @GetMapping("/jogging/{id}")
    public JoggingEntryResponse findById(@PathVariable long id) {
        return joggingService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        joggingService.deleteById(id);
    }


}
