package com.toptal.jogging.controller;

import java.util.List;

import com.toptal.jogging.response.JoggingAggregatedWeekResponse;
import com.toptal.jogging.service.JoggingReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jogging-report")
@RequiredArgsConstructor
public class JoggingReportController {

    private final JoggingReportService joggingReportService;

    @GetMapping("/joggings-weekly")
    public List<JoggingAggregatedWeekResponse> createWeeklyReport() {
        return joggingReportService.createWeeklyReport();
    }

    @GetMapping("/joggings-weekly-pageable")
    public Page<JoggingAggregatedWeekResponse> createWeeklyReport(Pageable pageable) {
        return joggingReportService.createWeeklyReport(pageable);
    }
}
