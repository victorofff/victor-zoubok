package com.toptal.jogging.controller;

import java.util.List;

import com.toptal.jogging.request.UserRequest;
import com.toptal.jogging.response.UserResponse;
import com.toptal.jogging.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class UsersController {
    private final UserService userService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse createUser(@Validated @RequestBody UserRequest userRequest) {
        return userService.save(userRequest);
    }

    @PutMapping("/{id}")
    public UserResponse updateUser(@PathVariable long id, @Validated @RequestBody UserRequest userRequest) {
        return userService.update(id, userRequest);
    }

    @GetMapping("/users")
    public List<UserResponse> findAll() {
        return userService.findAll();
    }

    @GetMapping("/users-pageable")
    public Page<UserResponse> findAllPageable(Pageable pageable) {
        return userService.findAllPageable(pageable);
    }

    @GetMapping("/user/{id}")
    public UserResponse findById(@PathVariable long id) {
        return userService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        userService.deleteById(id);
    }


}
