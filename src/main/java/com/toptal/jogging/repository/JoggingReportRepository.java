package com.toptal.jogging.repository;

import java.util.List;

import com.toptal.jogging.domain.Jogging;
import com.toptal.jogging.response.JoggingAggregatedWeekResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JoggingReportRepository extends JpaRepository<Jogging, Long> {

    @Query(nativeQuery = true,
        value = "select U.username as userName,"
            + " year(J.start_time) as year,"
            + " ISO_WEEK(J.start_time) as weekNumber,"
            + " AVG( CAST(  J.distance AS DOUBLE )) as averageMeters,"
            + " ( AVG( CAST(  J.distance AS DOUBLE )) / 1000.0 ) / (7 * 24) as averageSpeedKmHour"
            + " from JOGGING J INNER JOIN APP_USER U ON J.user_id = U.id"
            + " where (U.id = :userId or :userId < 0 )"
            + " group by U.username, year(J.start_time),  ISO_WEEK(J.start_time) order by year, weekNumber asc"
    )
    List<JoggingAggregatedWeekResponse> weeklyReportForUserId(@Param("userId") long userId);

    @Query(nativeQuery = true,
        value = "select U.username as userName,"
            + " year(J.start_time) as year,"
            + " ISO_WEEK(J.start_time) as weekNumber,"
            + " AVG( CAST(  J.distance AS DOUBLE )) as averageMeters,"
            + " ( AVG( CAST(  J.distance AS DOUBLE )) / 1000.0 ) / (7 * 24) as averageSpeedKmHour"
            + " from JOGGING J INNER JOIN APP_USER U ON J.user_id = U.id"
            + " where (U.id = :userId or :userId < 0)"
            + " group by U.username, year(J.start_time),  ISO_WEEK(J.start_time) order by year, weekNumber asc",

        // I removed average calculation to optimize DB performance however in case of complex queries it could be
        // difficult to preserve count when the logic is changed, so the only universal scenario I know is:
        // select count (1) from (<original query>) what is always not very good from DB performance
        countQuery = "select count(1) from "
            + "( select U.username,"
            + " year(J.start_time),"
            + " iso_week(J.start_time)"
            + " from JOGGING J INNER JOIN APP_USER U ON J.user_id = U.id"
            + " where (U.id = :userId or :userId < 0)"
            + " group by U.username, year(J.start_time),  iso_week(J.start_time) )"
    )
    Page<JoggingAggregatedWeekResponse> weeklyReportForUserId(@Param("userId") long userId, Pageable pageable);
}
