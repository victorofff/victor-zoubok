package com.toptal.jogging.repository;

import java.util.Optional;

import com.toptal.jogging.domain.Jogging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JoggingRepository extends JpaRepository<Jogging, Long>, JpaSpecificationExecutor<Jogging> {

    @Query("select jogging from Jogging jogging where jogging.id = :id and (:userId < 0L or jogging.user.id = :userId)")
    Optional<Jogging> findById(@Param("id") long id, @Param("userId") long userId);
}
