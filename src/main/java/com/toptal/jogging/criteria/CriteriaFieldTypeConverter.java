package com.toptal.jogging.criteria;


@FunctionalInterface
public interface CriteriaFieldTypeConverter<Y extends Comparable<? super Y>> {
    Y convertType(String value);
}
