package com.toptal.jogging.criteria.impl;

import com.toptal.jogging.criteria.CriteriaFieldTypeConverter;
import lombok.Getter;
import lombok.ToString;


@ToString
public class Token {
    public enum Type {
        OPEN_PARENTHESIS,
        CLOSE_PARENTHESIS,
        FIELD,
        EQ_OPERATOR,
        NE_OPERATOR,
        GT_OPERATOR,
        LT_OPERATOR,
        STRING_CONSTANT,
        NUMERICAL_CONSTANT,
        AND_OPERATOR,
        OR_OPERATOR
    }

    @Getter
    private final Token.Type type;

    @Getter
    private String value;

    private CriteriaFieldTypeConverter<?> criteriaFieldTypeConverter;

    public Token(Token.Type type) {
        this.type = type;
    }

    public Token(Token.Type type, String value) {
        this.type = type;
        this.value = value;
    }

    public Token createFieldTokenWithTypeConverter(CriteriaFieldTypeConverter<?> criteriaFieldTypeConverter) {
        if (!isField()) {
            throw new IllegalArgumentException("This operation is allowed only with FIELD tokens");
        }
        return new Token(type, value, criteriaFieldTypeConverter);
    }

    public boolean isOperand() {
        return type == Type.STRING_CONSTANT || type == Type.NUMERICAL_CONSTANT;
    }

    public boolean isField() {
        return type == Type.FIELD;
    }

    public boolean isOperator() {
        return type == Type.EQ_OPERATOR ||
            type == Type.NE_OPERATOR ||
            type == Type.GT_OPERATOR ||
            type == Type.LT_OPERATOR ||
            type == Type.AND_OPERATOR ||
            type == Type.OR_OPERATOR;
    }

    public boolean isLogicalOperator() {
        return type == Type.AND_OPERATOR || type == Type.OR_OPERATOR;
    }

    public boolean isLogicalAndOperator() {
        return type == Type.AND_OPERATOR;
    }

    public boolean isLogicalOrOperator() {
        return type == Type.OR_OPERATOR;
    }

    public boolean isWhereOperator() {
        return type == Type.EQ_OPERATOR ||
            type == Type.NE_OPERATOR ||
            type == Type.GT_OPERATOR ||
            type == Type.LT_OPERATOR;
    }


    @SuppressWarnings("unchecked")
    public <Y extends Comparable<? super Y>> CriteriaFieldTypeConverter<Y> getConverter() {
        if (criteriaFieldTypeConverter == null) {
            throw new IllegalArgumentException("Invalid converter");
        }
        return (CriteriaFieldTypeConverter<Y>) criteriaFieldTypeConverter;
    }

    private Token(Token.Type type, String value, CriteriaFieldTypeConverter<?> criteriaFieldTypeConverter) {
        this.type = type;
        this.value = value;
        this.criteriaFieldTypeConverter = criteriaFieldTypeConverter;
    }

}
