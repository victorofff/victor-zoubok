package com.toptal.jogging.criteria.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.Join;

import com.toptal.jogging.criteria.CriteriaFieldTypeConverter;
import com.toptal.jogging.criteria.CriteriaParser;
import com.toptal.jogging.criteria.impl.Token.Type;
import com.toptal.jogging.domain.User;
import com.toptal.jogging.util.Utility;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.NumberUtils;

@Component
@RequiredArgsConstructor
public class CriteriaParserImpl<E> implements CriteriaParser<E> {

    private static final String ALLOWED_FIELDS_PREFIX = "criteria.filter.";

    private final Environment environment;
    private final Deque<OperationResult<E>> values = new ArrayDeque<>();
    private final Deque<Token> operations = new ArrayDeque<>();

    @Value("${criteria.timestamp.format:yyyy-MM-dd HH:mm:ss.SSS}")
    private String timeStampFormatString;

    private DateTimeFormatter timeStampFormatter;

    @PostConstruct
    public void initialize() {
        timeStampFormatter = DateTimeFormatter.ofPattern(timeStampFormatString);
    }

    // the idea was grabbed from here: https://www.geeksforgeeks.org/expression-evaluation/
    public Specification<E> parseJoggingCriteria(long userId, String filter, Class<E> clazz) {

        if (StringUtils.isBlank(filter)) {
            return userId >= 0 ? getUserIdSpecification(userId) :
                (Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder.conjunction();
        }

        String fullPrefix = ALLOWED_FIELDS_PREFIX + clazz.getSimpleName().toLowerCase() + ".";
        Map<String, String> fieldsMetadata =
            Utility.getMultipleProperties(environment, fullPrefix).entrySet().stream()
                .collect(Collectors.toMap(entry -> StringUtils.remove(entry.getKey(), fullPrefix), Entry::getValue));

        Tokenizer tokenizer = new Tokenizer(filter);
        for (Token token : tokenizer) {

            if (token.isField()) {
                token = generateTokenWithConverter(fieldsMetadata, token);
            }

            if (token.isOperand() || token.isField()) {
                values.push(new OperationResult<>(token));
            } else if (token.getType() == Type.OPEN_PARENTHESIS) {
                operations.push(token);
            } else if (token.getType() == Type.CLOSE_PARENTHESIS) {
                while (!operations.isEmpty() && operations.peek().getType() != Type.OPEN_PARENTHESIS) {
                    values.push(applyOperation(operations.pop(), values.pop(), values.pop()));
                }
                operations.pop();
            } else if (token.isOperator()) {
                while (!operations.isEmpty() && hasPrecedence(token, operations.peek())) {
                    values.push(applyOperation(operations.pop(), values.pop(), values.pop()));
                }
                operations.push(token);
            }
        }

        while (!operations.isEmpty()) {
            values.push(applyOperation(operations.pop(), values.pop(), values.pop()));
        }

        OperationResult<E> result = values.pop();
        if (userId >= 0) {
            return applyOperation(new Token(Type.AND_OPERATOR), result,
                new OperationResult<>(getUserIdSpecification(userId))).getSpecification();
        } else {
            return result.getSpecification();
        }
    }

    // if op2 has higher or same precedence
    public static boolean hasPrecedence(Token op1, Token op2) {
        if (op2.getType() == Type.OPEN_PARENTHESIS || op2.getType() == Type.CLOSE_PARENTHESIS) {
            return false;
        }
        if (op1.isWhereOperator() && op2.isLogicalOperator()) {
            return false;
        }

        return !op1.isLogicalAndOperator() || !op2.isLogicalOrOperator();
    }

    private Specification<E> getUserIdSpecification(long userId) {
        return (Specification<E>) (root, query, criteriaBuilder) -> {
            // E must be joined to User
            Join<E, User> userJoin = root.join("user");
            return criteriaBuilder.equal(userJoin.get("id"), userId);
        };
    }

    private OperationResult<E> applyOperation(Token operation, OperationResult<E> left,
        OperationResult<E> right) {
        return left.operation(operation, right);
    }

    private Token generateTokenWithConverter(Map<String, String> fieldsMetadata, Token token) {
        String fieldType = fieldsMetadata.get(token.getValue());
        if (fieldType != null) {
            return token.createFieldTokenWithTypeConverter(createTypeConverter(fieldType, timeStampFormatter));
        }
        throw new IllegalArgumentException(
            String.format("Field %s is not declared for filtering criteria", token.getValue()));

    }

    @SuppressWarnings("unchecked")
    private static <Y extends Comparable<? super Y>> CriteriaFieldTypeConverter<Y> createTypeConverter(
        String fieldType, DateTimeFormatter dateTimeFormatter) {

        if (fieldType.equals(Integer.class.getName())) {
            return value -> (Y) NumberUtils.parseNumber(value, Integer.class);

        } else if (fieldType.equals(Long.class.getName())) {
            return value -> (Y) NumberUtils.parseNumber(value, Long.class);

        } else if (fieldType.equals(Double.class.getName())) {
            return value -> (Y) NumberUtils.parseNumber(value, Double.class);

        } else if (fieldType.equals(String.class.getName())) {
            return value -> (Y) value;

        } else if (fieldType.equals(Timestamp.class.getName())) {
            return value -> (Y) Timestamp.valueOf(LocalDateTime.parse(value, dateTimeFormatter));
        }

        throw new IllegalArgumentException(String.format("Cannot create type converter for type %s", fieldType));
    }


}
