package com.toptal.jogging.criteria.impl;

import com.toptal.jogging.criteria.CriteriaFieldTypeConverter;
import com.toptal.jogging.criteria.impl.Token.Type;
import lombok.Getter;
import org.springframework.data.jpa.domain.Specification;

@Getter
public class OperationResult<E> {

    private Token token;
    private Specification<E> specification;

    public OperationResult(Token token) {
        this.token = token;
    }

    public OperationResult(Specification<E> specification) {
        this.specification = specification;
    }

    public OperationResult<E> operation(Token operationToken, OperationResult<E> operand) {
        switch (operationToken.getType()) {
            case EQ_OPERATOR:
            case NE_OPERATOR:
            case GT_OPERATOR:
            case LT_OPERATOR:
                return new OperationResult<>(
                    createSpecification(getFieldToken(operand.getToken()), operationToken,
                        getValueToken(operand.getToken())));
            case AND_OPERATOR:
            case OR_OPERATOR:
                return new OperationResult<>(mergeSpecification(operand.getSpecification(), operationToken));
            default:
                break;
        }

        throw new IllegalArgumentException(String.format("Invalid operation %s", operationToken.toString()));
    }

    private Token getValueToken(Token anotherToken) {
        if (anotherToken.isOperand()) {
            return anotherToken;
        } else if (this.token.isOperand()) {
            return this.token;
        }

        throw new IllegalArgumentException(String.format("An operand must be of type: %s or %s",
            Type.NUMERICAL_CONSTANT, Type.STRING_CONSTANT));
    }

    private Token getFieldToken(Token anotherToken) {
        if (anotherToken.isField()) {
            return anotherToken;
        } else if (this.token.isField()) {
            return this.token;
        }
        throw new IllegalArgumentException(String.format("An operand must be of type: %s", Type.FIELD));
    }

    private Specification<E> mergeSpecification(Specification<E> right, Token operation) {
        switch (operation.getType()) {
            case OR_OPERATOR:
                return this.specification.or(right);
            case AND_OPERATOR:
                return this.specification.and(right);
            default:
                throw new IllegalArgumentException("Invalid logical operation");
        }
    }

    private <Y extends Comparable<? super Y>> Specification<E> createSpecification(
        Token fieldToken, Token operation, Token valueToken) {
        String field = fieldToken.getValue();
        CriteriaFieldTypeConverter<Y> fieldConverter = fieldToken.getConverter();
        String value = valueToken.getValue();

        switch (operation.getType()) {
            case EQ_OPERATOR:
                return (Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder
                    .equal(root.get(field), criteriaBuilder.literal(fieldConverter.convertType(value)));
            case NE_OPERATOR:
                return (Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder
                    .notEqual(root.get(field), criteriaBuilder.literal(fieldConverter.convertType(value)));
            case GT_OPERATOR:
                return (Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder
                    .greaterThanOrEqualTo(root.get(field), criteriaBuilder.literal(fieldConverter.convertType(value)));
            case LT_OPERATOR:
                return (Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder
                    .lessThanOrEqualTo(root.get(field), criteriaBuilder.literal(fieldConverter.convertType(value)));
            default:
                throw new IllegalArgumentException("Invalid operation within parenthesis");
        }
    }


}
