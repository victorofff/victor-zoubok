package com.toptal.jogging.criteria.impl;

import java.util.Iterator;
import java.util.StringTokenizer;

import com.toptal.jogging.criteria.impl.Token.Type;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;


public class Tokenizer implements Iterable<Token> {

    private static final String DELIMITERS = "() \t\n\r\f";

    private class TokenizerIterator implements Iterator<Token> {

        @Override
        public boolean hasNext() {
            return Tokenizer.this.hasNext();
        }

        @Override
        public Token next() {
            return Tokenizer.this.next();
        }
    }

    private final TokenizerIterator tokenizerIterator = new TokenizerIterator();
    private final StringTokenizer stringTokenizer;
    private Token current;

    public Tokenizer(String expression) {
        if (StringUtils.isBlank(expression)) {
            throw new IllegalArgumentException("Invalid expression");
        }
        stringTokenizer = new StringTokenizer(expression, DELIMITERS, true);
        current = nextIteration();
    }

    @Override
    public Iterator<Token> iterator() {
        return tokenizerIterator;
    }

    private boolean hasNext() {
        return current != null;
    }

    private Token next() {
        Token temp = current;
        current = nextIteration();
        return temp;
    }

    private Token nextIteration() {
        while (stringTokenizer.hasMoreElements()) {
            String stringToken = stringTokenizer.nextToken();

            if (DELIMITERS.contains(stringToken)) {
                if ("(".equals(stringToken)) {
                    return new Token(Type.OPEN_PARENTHESIS);
                } else if (")".equals(stringToken)) {
                    return new Token(Type.CLOSE_PARENTHESIS);
                }
            } else if ("eq".equalsIgnoreCase(stringToken)) {
                return new Token(Type.EQ_OPERATOR);
            } else if ("ne".equalsIgnoreCase(stringToken)) {
                return new Token(Type.NE_OPERATOR);
            } else if ("gt".equalsIgnoreCase(stringToken)) {
                return new Token(Type.GT_OPERATOR);
            } else if ("lt".equalsIgnoreCase(stringToken)) {
                return new Token(Type.LT_OPERATOR);
            } else if ("and".equalsIgnoreCase(stringToken)) {
                return new Token(Type.AND_OPERATOR);
            } else if ("or".equalsIgnoreCase(stringToken)) {
                return new Token(Type.OR_OPERATOR);
            } else if (StringUtils.startsWith(stringToken, "'")) {
                // extract until next '
                StringBuilder entireConstant = new StringBuilder(stringToken);
                while (stringTokenizer.hasMoreElements() && !StringUtils.endsWith(stringToken, "'")) {
                    stringToken = stringTokenizer.nextToken();
                    entireConstant.append(stringToken);
                }

                String stringConstantObtained = entireConstant.toString();
                if (!StringUtils.endsWith(stringConstantObtained, "'")) {
                   throw new IllegalArgumentException(String.format("Unterminated string constant %s", stringConstantObtained));
                }

                return new Token(Type.STRING_CONSTANT, StringUtils.strip(stringConstantObtained, "'"));
            } else if (NumberUtils.isParsable(stringToken)) {
                return new Token(Type.NUMERICAL_CONSTANT, stringToken);
            } else {
                return new Token(Type.FIELD, stringToken);
            }
        }

        return null;
    }


}
