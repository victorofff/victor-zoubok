package com.toptal.jogging.criteria;

import org.springframework.data.jpa.domain.Specification;

public interface CriteriaParser<E> {
    Specification<E> parseJoggingCriteria(long userId, String filter,  Class<E> clazz);
}
