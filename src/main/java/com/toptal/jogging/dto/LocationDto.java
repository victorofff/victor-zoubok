package com.toptal.jogging.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class LocationDto {

    @NotNull
    private Double latitude;

    @NotNull
    private Double longitude;
}
