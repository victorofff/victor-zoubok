package com.toptal.jogging.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Table(name = "app_privilege")
@EqualsAndHashCode(callSuper = true)
public class Privilege extends BaseDomain {

    @Column(name = "privilege_name", nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy = "privileges")
    private final Set<Role> roles = new HashSet<>();
}
