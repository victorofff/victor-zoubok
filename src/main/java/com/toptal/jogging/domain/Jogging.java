package com.toptal.jogging.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "jogging")
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"user"})
public class Jogging extends BaseDomain {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "start_time", nullable = false)
    private Timestamp timeStamp;

    @Column(name = "distance", nullable = false)
    private int distance;

    @Column(name = "weather_response")
    private String weatherResponse;

    @Embedded
    private Location location;
}
