package com.toptal.jogging.service;

import java.util.List;

import com.toptal.jogging.request.JoggingEntryRequest;
import com.toptal.jogging.response.JoggingEntryResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JoggingService {
    List<JoggingEntryResponse> findAll(String filter);

    Page<JoggingEntryResponse> findAllPageable(String filter, Pageable pageable);

    JoggingEntryResponse save(JoggingEntryRequest joggingEntryRequest);

    JoggingEntryResponse update(long id, JoggingEntryRequest joggingEntryRequest);

    void deleteById(long id);

    JoggingEntryResponse findById(long id);


}
