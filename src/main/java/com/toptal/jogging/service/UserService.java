package com.toptal.jogging.service;

import java.util.List;

import com.toptal.jogging.request.UserRequest;
import com.toptal.jogging.response.UserResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    Page<UserResponse> findAllPageable(Pageable pageable);

    List<UserResponse> findAll();

    UserResponse findById(long id);

    UserResponse save(UserRequest userRequest);

    UserResponse update(long id, UserRequest userRequest);

    void deleteById(long id);

}
