package com.toptal.jogging.service;

public class EntityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EntityNotFoundException(String entityClassName, long id) {
        super(String.format("Entity %s with id %d not found", entityClassName, id));
    }

    public EntityNotFoundException(String entityClassName, String businessId) {
        super(String.format("Entity %s with business id %s not found", entityClassName, businessId));
    }

    public EntityNotFoundException(Class<?> entityClass, long id) {
        this(entityClass.getName(), id);
    }

    public EntityNotFoundException(Class<?> entityClass, String businessId) {
        this(entityClass.getName(), businessId);
    }
}
