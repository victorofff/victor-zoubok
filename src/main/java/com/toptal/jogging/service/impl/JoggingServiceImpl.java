package com.toptal.jogging.service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import com.toptal.jogging.criteria.CriteriaParser;
import com.toptal.jogging.domain.Jogging;
import com.toptal.jogging.domain.Location;
import com.toptal.jogging.dto.LocationDto;
import com.toptal.jogging.repository.JoggingRepository;
import com.toptal.jogging.request.JoggingEntryRequest;
import com.toptal.jogging.request.ToEntity;
import com.toptal.jogging.response.FromEntity;
import com.toptal.jogging.response.JoggingEntryResponse;
import com.toptal.jogging.service.AuthenticationService;
import com.toptal.jogging.service.EntityNotFoundException;
import com.toptal.jogging.service.JoggingService;
import com.toptal.jogging.service.WeatherService;
import com.toptal.jogging.util.AuthenticationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class JoggingServiceImpl implements JoggingService {

    private final JoggingRepository joggingRepository;
    private final AuthenticationService authenticationService;
    private final WeatherService weatherService;
    private final CriteriaParser<Jogging> criteriaParser;

    private static final ToEntity<LocationDto, Location> LOCATION_DTO_TO_LOCATION =
        new ToEntity<LocationDto, Location>() {

            @Override
            public Location convert(LocationDto dtoObject) {
                Location location = new Location();

                location.setLatitude(dtoObject.getLatitude());
                location.setLongitude(dtoObject.getLongitude());

                return location;
            }

            @Override
            public void convert(Location location, LocationDto dtoObject) {
                location.setLatitude(dtoObject.getLatitude());
                location.setLongitude(dtoObject.getLongitude());
            }
        };


    private static final FromEntity<Location, LocationDto> LOCATION_TO_LOCATION_DTO =
        entityObject -> new LocationDto(entityObject.getLatitude(), entityObject.getLongitude());

    private static final FromEntity<Jogging, JoggingEntryResponse> JOGGING_TO_JOGGING_RESPONSE =
        entity -> JoggingEntryResponse
            .builder()
            .id(entity.getId())
            .distance(entity.getDistance())
            .localDateTime(entity.getTimeStamp().toLocalDateTime())
            .weatherJson(entity.getWeatherResponse())
            .userName(entity.getUser().getUserName())
            .locationDto(LOCATION_TO_LOCATION_DTO.convert(entity.getLocation()))
            .build();

    private final ToEntity<JoggingEntryRequest, Jogging> joggingRequestToJogging =
        new ToEntity<JoggingEntryRequest, Jogging>() {
            @Override
            public Jogging convert(JoggingEntryRequest dtoObject) {
                Jogging jogging = new Jogging();
                convert(jogging, dtoObject);
                return jogging;
            }

            @Override
            public void convert(Jogging jogging, JoggingEntryRequest dtoObject) {
                jogging.setUser(authenticationService.getUser());
                jogging.setDistance(dtoObject.getDistance());
                jogging.setTimeStamp(Timestamp.valueOf(dtoObject.getLocalDateTime()));
                jogging.setWeatherResponse(weatherService.getWeather(dtoObject.getLocationDto(),
                    dtoObject.getLocalDateTime()));
                jogging.setLocation(LOCATION_DTO_TO_LOCATION.convert(dtoObject.getLocationDto()));
            }
        };

    @Override
    @Transactional(readOnly = true)
    public Page<JoggingEntryResponse> findAllPageable(String filter, Pageable pageable) {
        Specification<Jogging> specification = criteriaParser
            .parseJoggingCriteria(AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService),
                filter, Jogging.class);

        if (specification == null) {
            throw new IllegalArgumentException("Invalid specification");
        }
        Page<Jogging> joggingPage = joggingRepository
            .findAll(specification, pageable);
        return joggingPage.map(JOGGING_TO_JOGGING_RESPONSE::convert);
    }

    @Override
    @Transactional(readOnly = true)
    public List<JoggingEntryResponse> findAll(String filter) {
        Specification<Jogging> specification = criteriaParser
            .parseJoggingCriteria(AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService),
                filter, Jogging.class);

        if (specification == null) {
            throw new IllegalArgumentException("Invalid specification");
        }
        return joggingRepository
            .findAll(specification).stream()
            .map(JOGGING_TO_JOGGING_RESPONSE::convert)
            .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public JoggingEntryResponse save(JoggingEntryRequest joggingEntryRequest) {
        Jogging jogging = joggingRequestToJogging.convert(joggingEntryRequest);
        return JOGGING_TO_JOGGING_RESPONSE.convert(joggingRepository.save(jogging));
    }

    @Transactional
    @Override
    public JoggingEntryResponse update(long id, JoggingEntryRequest joggingEntryRequest) {
        Jogging joggingReturned =
            joggingRepository
                .findById(id, AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService))
                .orElseThrow(() -> new EntityNotFoundException(Jogging.class, id));

        joggingRequestToJogging.convert(joggingReturned, joggingEntryRequest);
        return JOGGING_TO_JOGGING_RESPONSE.convert(joggingRepository.save(joggingReturned));
    }

    @Transactional
    @Override
    public void deleteById(long id) {
        Jogging joggingReturned =
            joggingRepository
                .findById(id, AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService))
                .orElseThrow(() -> new EntityNotFoundException(Jogging.class, id));

        joggingReturned.setUser(null);
        joggingRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public JoggingEntryResponse findById(long id) {
        return JOGGING_TO_JOGGING_RESPONSE.convert(joggingRepository
            .findById(id, AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService)).orElseThrow(
                () -> new EntityNotFoundException(Jogging.class, id)));
    }

}
