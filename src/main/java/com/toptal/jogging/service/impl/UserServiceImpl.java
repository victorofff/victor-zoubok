package com.toptal.jogging.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.toptal.jogging.domain.Jogging;
import com.toptal.jogging.domain.Role;
import com.toptal.jogging.domain.User;
import com.toptal.jogging.repository.RoleRepository;
import com.toptal.jogging.repository.UserRepository;
import com.toptal.jogging.request.ToEntity;
import com.toptal.jogging.request.UserRequest;
import com.toptal.jogging.response.FromEntity;
import com.toptal.jogging.response.UserResponse;
import com.toptal.jogging.service.AuthenticationService;
import com.toptal.jogging.service.EntityNotFoundException;
import com.toptal.jogging.service.UserService;
import com.toptal.jogging.util.AuthenticationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationService authenticationService;

    private final ToEntity<UserRequest, User> userRequestToUser = new ToEntity<UserRequest, User>() {

        @Override
        public User convert(UserRequest dtoObject) {
            User user = new User();
            convert(user, dtoObject);
            return user;
        }

        @Override
        public void convert(User user, UserRequest dtoObject) {
            user.setFirstName(dtoObject.getFirstName());
            user.setLastName(dtoObject.getLastName());
            user.setPassword(passwordEncoder.encode(dtoObject.getPassword()));
            user.setUserName(dtoObject.getUserName());

            user.getRoles().clear();
            for (String roleName : dtoObject.getRoles()) {
                Role role = roleRepository.findByRoleName(roleName);
                if (role == null) {
                    throw new EntityNotFoundException(Role.class, roleName);
                }
                user.getRoles().add(role);
            }
        }
    };

    private static final FromEntity<User, UserResponse> USER_TO_USER_RESPONSE = entity -> {
        UserResponse userResponse = new UserResponse(entity.getId(), entity.getUserName(),
            entity.getFirstName(),
            entity.getLastName());

        userResponse.getRoles().addAll(entity.getRoles().stream()
            .map(Role::getRoleName).collect(Collectors.toList()));

        return userResponse;
    };

    @Transactional
    @Override
    public UserResponse save(UserRequest userRequest) {
        AuthenticationUtil.checkRootAccess(authenticationService);

        User user = userRequestToUser.convert(userRequest);
        return USER_TO_USER_RESPONSE.convert(userRepository.save(user));
    }

    @Transactional
    @Override
    public UserResponse update(long id, UserRequest userRequest) {
        AuthenticationUtil.checkRootAccess(authenticationService);

        User userReturned =
            userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(User.class, id));
        userRequestToUser.convert(userReturned, userRequest);
        return USER_TO_USER_RESPONSE.convert(userRepository.save(userReturned));

    }

    @Override
    @Transactional(readOnly = true)
    public List<UserResponse> findAll() {
        AuthenticationUtil.checkRootAccess(authenticationService);

        return userRepository.findAll().stream().map(USER_TO_USER_RESPONSE::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponse> findAllPageable(Pageable pageable) {
        AuthenticationUtil.checkRootAccess(authenticationService);

        Page<User> userPage = userRepository.findAll(pageable);
        return userPage.map(USER_TO_USER_RESPONSE::convert);
    }

    @Transactional(readOnly = true)
    public UserResponse findById(long id) {
        AuthenticationUtil.checkRootAccess(authenticationService);

        return USER_TO_USER_RESPONSE.convert(userRepository.findById(id).orElseThrow(
            () -> new EntityNotFoundException(Jogging.class, id)));
    }


    @Transactional
    @Override
    public void deleteById(long id) {
        AuthenticationUtil.checkRootAccess(authenticationService);

        User userReturned =
            userRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(User.class, id));

        userRepository.deleteById(userReturned.getId());
    }


}
