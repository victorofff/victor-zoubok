package com.toptal.jogging.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.toptal.jogging.domain.Privilege;
import com.toptal.jogging.domain.Role;
import com.toptal.jogging.domain.User;
import com.toptal.jogging.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ApplicationUserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(userName);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", userName));
        }

        return new org.springframework.security.core.userdetails.User(
            user.getUserName(), user.getPassword(), true, true, true,
            true, getGrantedAuthorities(getPrivileges(user.getRoles())));
    }

    private Set<Privilege> getPrivileges(Set<Role> roles) {
        return roles.stream().flatMap(role -> role.getPrivileges().stream()).collect(Collectors.toSet());
    }

    private List<GrantedAuthority> getGrantedAuthorities(Set<Privilege> privileges) {
        return privileges.stream()
            .map(privilege -> new SimpleGrantedAuthority(privilege.getName())).collect(Collectors.toList());
    }
}
