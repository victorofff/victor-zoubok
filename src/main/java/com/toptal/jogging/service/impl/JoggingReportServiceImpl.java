package com.toptal.jogging.service.impl;

import java.util.List;

import com.toptal.jogging.repository.JoggingReportRepository;
import com.toptal.jogging.response.JoggingAggregatedWeekResponse;
import com.toptal.jogging.service.AuthenticationService;
import com.toptal.jogging.service.JoggingReportService;
import com.toptal.jogging.util.AuthenticationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class JoggingReportServiceImpl implements JoggingReportService {

    private final JoggingReportRepository joggingReportRepository;
    private final AuthenticationService authenticationService;

    @Override
    @Transactional(readOnly = true)
    public List<JoggingAggregatedWeekResponse> createWeeklyReport() {
        return joggingReportRepository
            .weeklyReportForUserId(AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<JoggingAggregatedWeekResponse> createWeeklyReport(Pageable pageable) {
        return joggingReportRepository
            .weeklyReportForUserId(AuthenticationUtil.resolveUserIdForManagerOrStandardUser(authenticationService),
                pageable);
    }

}
