package com.toptal.jogging.service.impl;

import com.toptal.jogging.domain.User;
import com.toptal.jogging.repository.UserRepository;
import com.toptal.jogging.service.AuthenticationService;
import com.toptal.jogging.service.NonAuthorizedException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;

    @Override
    public boolean hasPrivilege(String role) {

        if (StringUtils.isBlank(role)) {
            throw new IllegalArgumentException("Invalid privilege");
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new NonAuthorizedException("Not authorized");
        }

        return authentication.getAuthorities().stream().anyMatch(a -> role.equals(a.getAuthority()));
    }

    @Override
    public User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new NonAuthorizedException("Not authorized");
        }
        return resolveUser(resolveUserName(authentication));
    }

    private User resolveUser(String userName) {
        User user = userRepository.findByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", userName));
        }
        return user;
    }

    private static String resolveUserName(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        if (principal instanceof String) {
            return (String) principal;
        } else if (principal instanceof org.springframework.security.core.userdetails.User) {
            org.springframework.security.core.userdetails.User springUser =
                (org.springframework.security.core.userdetails.User) principal;
            return springUser.getUsername();
        } else {
            throw new IllegalArgumentException("Authentication Principal type is not supported");
        }
    }

}
