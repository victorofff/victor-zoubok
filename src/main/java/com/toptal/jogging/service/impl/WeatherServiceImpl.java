package com.toptal.jogging.service.impl;

import java.time.LocalDateTime;

import com.toptal.jogging.dto.LocationDto;
import com.toptal.jogging.service.WeatherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@RequiredArgsConstructor
@Service
public class WeatherServiceImpl implements WeatherService {

    @Value("${wheather.api.service.url}")
    private String serviceUrl;

    @Value("${whether.api.key}")
    private String apiKey;

    private final RestTemplate restTemplate;

    @Override
    public String getWeather(LocationDto locationDto, LocalDateTime localDateTime) {
        // https://samples.openweathermap.org/data/2.5/weather?lat=37&lon=280&appid=b6907d289e10d714a6e88b30761fae22
        String uri = UriComponentsBuilder.fromHttpUrl(serviceUrl)
            .queryParam("lat", locationDto.getLatitude())
            .queryParam("lon", locationDto.getLongitude())
            .queryParam("appid", apiKey)
            .toUriString();

        try {
            log.info("Retrieve weather data for location: {}, time: {}", locationDto.toString(),
                localDateTime.toString());
            return restTemplate.getForObject(uri, String.class);

        } catch (RestClientException ex) {
            log.error("Exception while retrieving weather information", ex);
        }

        return "";
    }

}
