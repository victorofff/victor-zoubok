package com.toptal.jogging.service;

import java.util.List;

import com.toptal.jogging.response.JoggingAggregatedWeekResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JoggingReportService {
    List<JoggingAggregatedWeekResponse> createWeeklyReport();

    Page<JoggingAggregatedWeekResponse> createWeeklyReport(Pageable pageable);
}
