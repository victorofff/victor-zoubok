package com.toptal.jogging.service;

import com.toptal.jogging.domain.User;

public interface AuthenticationService {
    boolean hasPrivilege(String privilege);

    User getUser();
}
