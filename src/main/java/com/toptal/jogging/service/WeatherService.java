package com.toptal.jogging.service;

import java.time.LocalDateTime;

import com.toptal.jogging.dto.LocationDto;

public interface WeatherService {
    String getWeather(LocationDto locationDto, LocalDateTime localDateTime);
}
