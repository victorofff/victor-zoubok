package com.toptal.jogging.service;

public class NonAuthorizedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NonAuthorizedException(String message) {
        super(message);
    }

}
