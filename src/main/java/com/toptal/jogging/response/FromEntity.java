package com.toptal.jogging.response;

public interface FromEntity<ENTITY, DTO> {
    DTO convert(ENTITY entityObject);
}
