package com.toptal.jogging.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public interface JoggingAggregatedWeekResponse {
    String getUserName();

    int getYear();

    int getWeekNumber();

    @JsonSerialize(using = JoggingAggregatedReportDoubleSerializer.class)
    double getAverageSpeedKmHour();

    @JsonSerialize(using = JoggingAggregatedReportDoubleSerializer.class)
    double getAverageMeters();
}
