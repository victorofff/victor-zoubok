package com.toptal.jogging.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.toptal.jogging.config.ConfigConst;
import com.toptal.jogging.dto.LocationDto;
import lombok.Builder;
import lombok.Getter;


@Builder
@Getter
public class JoggingEntryResponse {
    private final long id;

    @JsonFormat(pattern = ConfigConst.GENERAL_DATETIME_FORMAT)
    private final LocalDateTime localDateTime;

    private final int distance;

    private final LocationDto locationDto;

    private final String weatherJson;

    private final String userName;
}
