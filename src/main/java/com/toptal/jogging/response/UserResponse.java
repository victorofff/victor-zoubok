package com.toptal.jogging.response;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class UserResponse {
    private final long id;
    private final String userName;
    private final String firstName;
    private final String lastName;
    private final Set<String> roles = new HashSet<>();
}
