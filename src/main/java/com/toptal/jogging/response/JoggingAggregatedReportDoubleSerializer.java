package com.toptal.jogging.response;

import java.io.IOException;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.toptal.jogging.config.ConfigConst;

public class JoggingAggregatedReportDoubleSerializer extends JsonSerializer<Double> {
    @Override
    public void serialize(Double value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
        generator.writeNumber(String.format(Locale.US, ConfigConst.REPORT_CONVERT_DOUBLE_TO_STRING_FORMAT, value));
    }
}
