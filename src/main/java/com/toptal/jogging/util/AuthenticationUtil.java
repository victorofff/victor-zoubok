package com.toptal.jogging.util;

import com.toptal.jogging.service.AuthenticationService;
import com.toptal.jogging.service.NonAuthorizedException;

public class AuthenticationUtil {
    private AuthenticationUtil() {
    }

    public static long resolveUserIdForManagerOrStandardUser(AuthenticationService authenticationService) {
        if (authenticationService.hasPrivilege("CRUD_ALL_USERS")) {
            return -1;
        } else if (authenticationService.hasPrivilege("CRUD_OWN_USERS")) {
            return authenticationService.getUser().getId();
        }
        throw new NonAuthorizedException("User privilege must be either 'CRUD_OWN_USERS' or 'CRUD_ALL_USERS'");
    }

    public static void checkRootAccess(AuthenticationService authenticationService) {
        if (!authenticationService.hasPrivilege("CRUD_ALL_RECORDS")) {
            throw new NonAuthorizedException("Access allowed only for user owing the privilege 'CRUD_ALL_RECORDS'");
        }
    }

}
