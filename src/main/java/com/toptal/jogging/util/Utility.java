package com.toptal.jogging.util;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;

public class Utility {
    private Utility() {
    }

    public static Map<String, String> getMultipleProperties(Environment environment, String prefix) {
        if (environment instanceof ConfigurableEnvironment) {
            ConfigurableEnvironment configurableEnvironment = (ConfigurableEnvironment) environment;

            Set<String> keys = configurableEnvironment.getPropertySources().stream()
                .filter(propertySource -> propertySource instanceof EnumerablePropertySource)
                .map(propertySource -> (EnumerablePropertySource<?>) propertySource)
                .flatMap(propertySource -> Stream.of(propertySource.getPropertyNames()))
                .filter(key -> key.startsWith(prefix))
                .collect(Collectors.toSet());

            return Collections.unmodifiableMap(
                keys.stream().collect(Collectors.toMap(key -> key, configurableEnvironment::getProperty)));
        }

        return Collections.emptyMap();
    }

}
