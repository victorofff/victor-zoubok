package com.toptal.jogging.request;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.toptal.jogging.config.ConfigConst;
import com.toptal.jogging.dto.LocationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JoggingEntryRequest {
    @JsonDeserialize(using = JoggingEntryRequestLocalDateTimeDeserializer.class)
    @JsonFormat(pattern = ConfigConst.GENERAL_DATETIME_FORMAT)
    private LocalDateTime localDateTime;

    @Positive
    private int distance;

    @NotNull
    @Valid
    private LocationDto locationDto;
}
