package com.toptal.jogging.request;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.toptal.jogging.config.ConfigConst;

public class JoggingEntryRequestLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    private static final DateTimeFormatter DEFAULT_FORMATTER =
        DateTimeFormatter.ofPattern(ConfigConst.GENERAL_DATETIME_FORMAT);

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext context)
        throws IOException {
        String dateTime = jsonParser.getText();
        return LocalDateTime.parse(dateTime, DEFAULT_FORMATTER);
    }
}
