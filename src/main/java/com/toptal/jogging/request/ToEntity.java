package com.toptal.jogging.request;

public interface ToEntity<DTO, ENTITY> {
    ENTITY convert(DTO dtoObject);
    void convert(ENTITY entity, DTO dtoObject);
}
