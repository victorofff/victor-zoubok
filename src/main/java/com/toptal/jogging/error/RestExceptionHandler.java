package com.toptal.jogging.error;

import java.util.stream.Collectors;

import com.toptal.jogging.config.ConfigConst;
import com.toptal.jogging.service.EntityNotFoundException;
import com.toptal.jogging.service.NonAuthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.MimeType;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
        MissingServletRequestParameterException missingServletRequestParameterException,
        HttpHeaders headers, HttpStatus status, WebRequest request) {

        return buildResponseEntity(new ApiError(BAD_REQUEST,
            missingServletRequestParameterException.getParameterName() + " parameter is missing",
            missingServletRequestParameterException));
    }


    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
        HttpMediaTypeNotSupportedException httpMediaTypeNotSupportedException,
        HttpHeaders headers, HttpStatus status, WebRequest request) {

        return buildResponseEntity(
            new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, httpMediaTypeNotSupportedException.getContentType() +
                " media type is not supported. Supported media types are " +
                httpMediaTypeNotSupportedException.getSupportedMediaTypes().stream().map(MimeType::toString)
                    .collect(Collectors.joining(", ")),
                httpMediaTypeNotSupportedException));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException methodArgumentNotValidException,
        HttpHeaders headers, HttpStatus status, WebRequest request) {

        return buildResponseEntity(new ApiError(BAD_REQUEST, ConfigConst.VALIDATION_ERROR));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        HttpMessageNotReadableException httpMessageNotReadableException,
        HttpHeaders headers, HttpStatus status, WebRequest request) {

        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        log.error("{} to {}", servletWebRequest.getHttpMethod(), servletWebRequest.getRequest().getServletPath());
        return buildResponseEntity(
            new ApiError(HttpStatus.BAD_REQUEST, ConfigConst.MALFORMED_JSON_REQUEST, httpMessageNotReadableException));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(
        HttpMessageNotWritableException httpMessageNotWritableException,
        HttpHeaders headers, HttpStatus status, WebRequest request) {
        return buildResponseEntity(
            new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ConfigConst.ERROR_WRITING_JSON_OUTPUT, httpMessageNotWritableException));
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
        NoHandlerFoundException noHandlerFoundException, HttpHeaders headers, HttpStatus status, WebRequest request) {

        return buildResponseEntity(new ApiError(BAD_REQUEST,
            String.format("Could not find the %s method for URL %s", noHandlerFoundException.getHttpMethod(),
                noHandlerFoundException.getRequestURL()), noHandlerFoundException));
    }


    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
        javax.validation.ConstraintViolationException constraintViolationException) {
        return buildResponseEntity(new ApiError(BAD_REQUEST, ConfigConst.VALIDATION_ERROR, constraintViolationException));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException entityNotFoundException) {
        return buildResponseEntity(new ApiError(NOT_FOUND, ConfigConst.NOT_FOUND_EXCEPTION, entityNotFoundException));
    }

    @ExceptionHandler(NonAuthorizedException.class)
    protected ResponseEntity<Object> handleNonAuthorized(NonAuthorizedException nonAuthorizedException) {
        return buildResponseEntity(new ApiError(HttpStatus.UNAUTHORIZED, ConfigConst.UNAUTHORIZED, nonAuthorizedException));
    }


    @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(
        javax.persistence.EntityNotFoundException entityNotFoundException) {
        return buildResponseEntity(new ApiError(NOT_FOUND, ConfigConst.NOT_FOUND_EXCEPTION, entityNotFoundException));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(
        DataIntegrityViolationException dataIntegrityViolationException, WebRequest request) {
        if (dataIntegrityViolationException.getCause() instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationException =
                (ConstraintViolationException) dataIntegrityViolationException
                    .getCause();
            return buildResponseEntity(
                new ApiError(HttpStatus.CONFLICT,
                    ExceptionUtils.getMessage(constraintViolationException.getSQLException()),
                    dataIntegrityViolationException.getCause()));
        }
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ConfigConst.INTERNAL_ERROR,
            dataIntegrityViolationException));
    }


    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(
        MethodArgumentTypeMismatchException methodArgumentTypeMismatchException, WebRequest request) {

        return buildResponseEntity(new ApiError(BAD_REQUEST, String
            .format("The parameter '%s' of value '%s' could not be converted to type '%s'",
                methodArgumentTypeMismatchException.getName(), methodArgumentTypeMismatchException.getValue(),
                methodArgumentTypeMismatchException.getRequiredType().getSimpleName()),
            methodArgumentTypeMismatchException));
    }

    @ExceptionHandler(AccessDeniedException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(AccessDeniedException accessDeniedException,
        WebRequest request) {
        return buildResponseEntity(new ApiError(HttpStatus.UNAUTHORIZED, ConfigConst.UNAUTHORIZED, accessDeniedException));
    }

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<Object> handleRemainingExceptions(
        RuntimeException runTimeException, WebRequest request) {
        return buildResponseEntity(
            new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ConfigConst.UNEXPECTED_EXCEPTION, runTimeException));
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
