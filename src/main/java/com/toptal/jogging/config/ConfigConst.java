package com.toptal.jogging.config;

public class ConfigConst {
    private ConfigConst() {
    }

    public static final String UNEXPECTED_EXCEPTION = "Unexpected exception";
    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String INTERNAL_ERROR = "Internal error";
    public static final String NOT_FOUND_EXCEPTION = "Not found exception";
    public static final String VALIDATION_ERROR = "Validation error";
    public static final String ERROR_WRITING_JSON_OUTPUT = "Error writing JSON output";
    public static final String MALFORMED_JSON_REQUEST = "Malformed JSON request";

    public static final String RESOURCE_ID = "api";
    public static final String GENERAL_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String READ_SCOPE = "read";
    public static final String WRITE_SCOPE = "write";
    public static final String PASSWORD_GRANT_TYPE = "password";
    public static final String REPORT_CONVERT_DOUBLE_TO_STRING_FORMAT = "%.6f";
}
