package com.toptal.jogging.config;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;


public class CustomOauthExceptionSerializer extends StdSerializer<CustomOauthException> {

    private final ObjectMapper mapper = Jackson2ObjectMapperBuilder.json().build();

    public CustomOauthExceptionSerializer() {
        super(CustomOauthException.class);
    }

    @Override
    public void serialize(CustomOauthException customOauthException, JsonGenerator jsonGenerator,
        SerializerProvider provider) throws IOException {
        jsonGenerator.writeRaw(mapper.writeValueAsString(customOauthException.getApiError()));
    }
}
