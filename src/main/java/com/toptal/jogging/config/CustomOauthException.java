package com.toptal.jogging.config;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.toptal.jogging.error.ApiError;
import lombok.Getter;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

@Getter
@JsonSerialize(using = CustomOauthExceptionSerializer.class)
public class CustomOauthException extends OAuth2Exception {
    private static final long serialVersionUID = 1L;

    private final ApiError apiError;

    public CustomOauthException(ApiError apiError) {
        super(apiError.getMessage());
        this.apiError = apiError;
    }
}
