package com.toptal.jogging.config;

import com.toptal.jogging.error.ApiError;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public class CustomResponseExceptionTranslator implements WebResponseExceptionTranslator<OAuth2Exception> {
    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception exception) {
        ApiError apiError = new ApiError(UNAUTHORIZED, ConfigConst.UNAUTHORIZED, exception);
        return new ResponseEntity<>(new CustomOauthException(apiError), apiError.getStatus());
    }
}
