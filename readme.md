# FORMAL DEADLINE: 
*Assignment email received on Wed 2/5/2020 12:37 AM PST + 2 weeks - 12 hours:*

**Tue 2/18/2020 12:37 PM PST**


## Design Considerations

*> API Users must be able to create an account and log in.*

JWT authentication is used. We setup our application as Authentication Server and obtain JWT tokens with password 
grant type to provide it further with other REST API calls. See ```ComplexScenarioTest```;

*> All API calls must be authenticated.*

All API calls are accompanied with ```Authorization``` header containing ```Bearer <JWT token>``` 

*> Implement at least three roles with different permission levels: a regular user would only be able to CRUD on their owned records, a user manager would be able to CRUD only users, and an admin would be able to CRUD all records and users.*

There are free users created on Application startup:
1. **john.doe** having the role **STANDARD_USER**
2. **bill.robson** having the role **MANAGER_USER**
3. **bob.frapples** having the role **ADMIN_USER**

Each role is a coarse-grained authorization control. Each role can contain one or more privileges 
what are Spring **authorities**. We have the following privileges:

1. **CRUD_OWN_USERS** - it permits to CRUD only own jogging records (reads, deletes, writes, updates are performed as from
currently logged user, if we attempt to remove or read a record belonging another user, 
then Application raises `NOT_FOUND` response).

2. **CRUD_ALL_USERS** - it permits to CRUD all jogging records.

3. **CRUD_ALL_RECORDS** - it permits to CRUD user and jogging records, so this privilege is necessary to create a new user. 

*> Each time entry when entered has a date, distance, time, and location.*

We record a jogging event datetime, distance and location coordinates.

*> Based on the provided date and location, API should connect to a weather API provider and get the weather conditions for the run, and store that with each run.*

I found some demo weather API: https://samples.openweathermap.org/data/2.5/weather. It is queried per each
insert into jogging table.

*> The API must create a report on average speed & distance per week.*

I created a separate controller ```JoggingReportController``` and a separate service ```JoggingReportService```
because CRUD and reporting tasks are quite different (technically and functionally) and 
it is a good practice to split them.

*> The API must be able to return data in the JSON format.*

This is REST API with JSON responses. 

*The API should provide filter capabilities for all endpoints that return a list of elements, as well should be able to support pagination.*

1. ```CriteriaParser``` is responsible for parsing infix expressions. Assumptions:
    * There are 4 filtering operators available: 
        * `eq` - equal;
        * `ne`- not equal; 
        * `gt` - greater then or equal;
        * `lt` - less the or equal;
    * Filtering operators form an expression:
    `<field name>` `<filtering operator>` `<constant>`
    * `<field name>` is expected to be an allowed entity field name, see **application.properties**,  
    ```criteria.filter.jogging.<field name>``` properties;**(1)**
    *  `constant` can be **Numerical** or **String**. It is converted to a relevant Java type via **(1)** mapping;    
    * The above operators have the highest precedence. Logical operators can be applied filtering expressions:
        * `or` - Logical disjunction;
        * `and` - logical conjunction;
        They have lower precedence than filtering operators.
    * Parenthesis are used to change the precedence of operators;
    * `criteria.timestamp.format=<date time format>` is used to define a relevant conversion 
    to TimeStamp fields;  
    * Lists and pagination are supported for all controllers, filtering is supported only for ```JoggingController```.
                              

*> The API filtering should allow using parenthesis for defining operations precedence and use any combination of the available fields. The supported operations should at least include or, and, eq (equals), ne (not equals), gt (greater than), lt (lower than).
Example -> (date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10)).*

Please see the above point.

*> Write unit and e2e tests.*

1. As unit tests I used ```JUnit 4```;
2. Services and controllers were tested using ```@SpringBootTest```;
3. **Spring Cloud Contract** was used for e2e tests; 

## Coding Considerations

1. In real project we probably should query Weather service asynchronously or use some message broker.

2. JWT tokens authentication is not complete, in real project we should use refresh tokens, and not keep them
in memory to avoid tokens invalidations after Application restart. Also logout should be implemented.  

3. I used H2 in memory database for both production and testing code. For sure we should use another RDBMS like
PostgreSQL or MySQL for real project. 
    * I don't see any No-SQL application here as:
        * We have a pretty well designed schema. Weather responses probably could be an exception but we can map
     important attributes like temperature or wind speed to relevant attributes for further search;
        * There is no requirement to sharding unlike if we would keep jogging events for all people across a big country;
        * RDBMS is a very good in keeping ACID, no need to avoid this;   
    * I used ```GenerationType.IDENTITY``` though in real project sequence probably should be preferred;
    * I did not create FK indexes however in a big PROD DB a good idea to manage indexes storage explicitly;
    * I added simple ```version``` attribute assuming optimistic locking, though in real project this should be clarified
    by analysing use-cases;         
 
4. I prefer to implement base classes for testing to keep common things, 
though in a real project we need to be cautious not to pull unnecessary Spring context. 

5. For filtering functionality I used ```JpaSpecificationExecutor``` with **Criteria API** 
and **Specification**. Note that ```CriteriaParserImpl<E>``` accepts entities joined to ```User```
because it needs to filetr our own users if necessary.

6. I prefer to use request and response DTO objects and do not allow clients to operate with entities
directly. 

7. I did not implement sorting excepting for ```JoggingReportController``` because it is 
natural for reports.  

8. Lists and pagination are supported for all controllers, filtering is supported only for ```JoggingController```
This is because I did not expect a very complex queries for users. For reports probably it could make sense but only
for a very limited number of criteria like an observation period or a list of allowed users and it may not directly match
to entity attributes.
   
9. ```Jogging``` -> ```User``` is only ```@ManyToOne``` because we don't need query User about joggings, also
the number of jogging records could be extremely high. 

10. There are checks for granted authorities at service layer as well. It is probably excessive here
but in a real project we will need have an ability to avoid non-authorized functionality
from service layer.
   

