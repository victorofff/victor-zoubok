FROM openjdk:8-jdk-alpine
LABEL maintainer="victorofff@gmail.com"
EXPOSE 8080
ARG JAR_FILE=target/jogging-api-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} jogging-api.jar
ENTRYPOINT ["java","-jar","/jogging-api.jar"]